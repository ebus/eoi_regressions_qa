package com.qa.EOI.SSOusers;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.CoverageChangeRequestPageAction;
import com.qa.pageActions.DemoDocumentSignPageAction;
import com.qa.pageActions.HealthQuestions1PageAction;
import com.qa.pageActions.HealthQuestions2PageAction;
import com.qa.pageActions.HealthQuestions3PageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PersonalInformationPageAction;
import com.qa.pageActions.ReviewAndSignPageAction;
import com.qa.pageActions.UpdateSecurityInformationPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class EmpOnly_ApprovedStatusSSOTest extends TestBase {
	public EmpOnly_ApprovedStatusSSOTest() {
		super();
	}
	
/* *****************************************************************************
	  * Test Name : Approved EOI Status for Employee Only SSO user
	  * Purpose : To validate the status is getting updated to Approved
	  * History : Created by Pallavi Gopinath on 10/27/2021 	 
*******************************************************************************/

	GenericFunction genericFunction = new GenericFunction();
	LoginPageAction loginPageAction;
	UpdateSecurityInformationPageAction updateSecurityInformationPageAction;
	HomePageAction homePageAction;
	PersonalInformationPageAction personalInformationPageAction;
	CoverageChangeRequestPageAction coverageChangeRequestPageAction;
	HealthQuestions1PageAction healthQuestions1PageAction;
	HealthQuestions2PageAction healthQuestions2PageAction;
	HealthQuestions3PageAction healthQuestions3PageAction;
	ReviewAndSignPageAction reviewAndSignPageAction;
	DemoDocumentSignPageAction demoDocumentSignPageAction;
	ConfirmationPageAction confirmationPageAction;


	@DataProvider
	public Object[][] getApprovedStatus() {
		Object data[][] = TestUtil.getTestData("SSO_EmpOnly_Approved");
		return data;
	}

	@Test(priority=1, dataProvider="getApprovedStatus")
	public void verifyApprovedStatus(String userId, String passWord,String role, String ssnVal, String heightFt, String heightInch,
			String weightLbs, String validTempPwd, String LessThan8Chrs, String MoreThan20Chrs, String AlphanumericandCaseChrs,
			String PwdwithInvalidSpclChr, String PwdwithUserID, String PwdwithSpace, String validPwd, String invalidConfirmPwd,
    		String validConfirmPwd) throws Exception {
	
			Thread.sleep(3000);
			extentTest = extent.startTest("Verifying Approved EOI status for " + role + " SSO User");
			intialization();
			extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
			loginPageAction = new LoginPageAction();
//			if update security page is not required then comment line 65, 66 and uncomment 68
//			updateSecurityInformationPageAction= loginPageAction.userLogin(userId, passWord);//User will login using this function
//			homePageAction=updateSecurityInformationPageAction.validateSecurityInformation(validTempPwd,LessThan8Chrs,MoreThan20Chrs,AlphanumericandCaseChrs,PwdwithInvalidSpclChr,PwdwithUserID,PwdwithSpace,validPwd, invalidConfirmPwd, validConfirmPwd);
			
			homePageAction = loginPageAction.userLoginToHomePage(userId, passWord);//User will login using this function
			personalInformationPageAction = homePageAction.validateHomepage();  //New User, Begin EOI button
			//personalInformationPageAction = homePageAction.validateHomePageContEOI();   //For Scripting and debugging purpose
			coverageChangeRequestPageAction = personalInformationPageAction.verifyPersonalInfoSSO(ssnVal, heightFt, heightInch, weightLbs);
			healthQuestions1PageAction = coverageChangeRequestPageAction.verifyCoverageSSO();
			healthQuestions2PageAction = healthQuestions1PageAction.validatePreApprovedQuestionSelection1();
			healthQuestions3PageAction = healthQuestions2PageAction.validatePreApprovedQuestions2();
			reviewAndSignPageAction = healthQuestions3PageAction.validatePreApprovedQuestions3();
			demoDocumentSignPageAction = reviewAndSignPageAction.validateReviewPage();
			confirmationPageAction = demoDocumentSignPageAction.validateSign();
			homePageAction = confirmationPageAction.validateApprovedConfirmationSSO();
			homePageAction.validateApprovedConfirmationStatusSSO();
			homePageAction.logOut();
			
		}

}
