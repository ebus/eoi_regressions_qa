package com.qa.EOI.NonSSOusers;

import java.io.IOException;

import org.apache.commons.exec.LogOutputStream;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.CoverageChangeRequestPageAction;
import com.qa.pageActions.DemoDocumentSignPageAction;
import com.qa.pageActions.HealthQuestions1PageAction;
import com.qa.pageActions.HealthQuestions2PageAction;
import com.qa.pageActions.HealthQuestions3PageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PersonalInformationPageAction;
import com.qa.pageActions.ReviewAndSignPageAction;
import com.qa.pageActions.UpdateSecurityInformationPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class ReferredTest extends TestBase{
	public ReferredTest() {
		super();	
	}
	
/* *****************************************************************************
	  * Test Name : Referred Status of an Employee with Multiple Questions
	  * Purpose : To validate the status is getting updated to Referred
	  * History : Created by Anjali Johny on 07/29/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
UpdateSecurityInformationPageAction updateSecurityInformationPageAction;
HomePageAction homePageAction;
PersonalInformationPageAction personalInformationPageAction; 
CoverageChangeRequestPageAction coverageChangeRequestPageAction;
HealthQuestions1PageAction healthQuestions1PageAction ;
HealthQuestions2PageAction healthQuestions2PageAction ;
HealthQuestions3PageAction healthQuestions3PageAction;
ReviewAndSignPageAction reviewAndSignPageAction;
DemoDocumentSignPageAction demoDocumentSignPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getReferredMultipleQuestionStatus(){
	Object data[][] = TestUtil.getTestData("NonSSO_ReferredMultipleQuestion");
	return data;
}


@Test(priority=1,dataProvider="getReferredMultipleQuestionStatus")
    public void ReferredStatus(String userId, String passWord,String role,String validTempPwd,String LessThan8Chrs,String MoreThan20Chrs,
    		String AlphanumericandCaseChrs,String PwdwithInvalidSpclChr,String PwdwithUserID,String PwdwithSpace,String validPwd,String invalidConfirmPwd,
    		String validConfirmPwd,String fname,String midname,String lname,String ssn,String homeAddr,String city,String state,String zip,String gender,String dob,String Martialstatus,
    		String heightfeet,String heightinch,String weight,String doh,String annualsalary,String ph,String type,String email,String CurrentCvgAmnt,String ElectedCvgAmnt,String AddtionalDetail,String NameMedication,String DateOfUse,String DatePrescribed,String NameAddressOfPrescribed) throws Exception {
    
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Referred status with Multiple Question Selection for "+role);
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	//if update security page is not required then comment line 68,69 and uncomment 70
	
	//updateSecurityInformationPageAction= loginPageAction.userLogin(userId, passWord);//User will login using this function
	//homePageAction=updateSecurityInformationPageAction.validateSecurityInformation(validTempPwd,LessThan8Chrs,MoreThan20Chrs,AlphanumericandCaseChrs,PwdwithInvalidSpclChr,PwdwithUserID,PwdwithSpace,validPwd, invalidConfirmPwd, validConfirmPwd);
	homePageAction=loginPageAction.userLoginToHomePage(userId, passWord);//User will login using this function
	personalInformationPageAction=homePageAction.validateHomepage();
	coverageChangeRequestPageAction=personalInformationPageAction.validatePersonalInfo(fname,midname,lname,ssn,homeAddr,city,state,zip,gender,dob,Martialstatus,heightfeet,heightinch,weight,doh,
		annualsalary,ph,type,email);
	healthQuestions1PageAction=coverageChangeRequestPageAction.validateCoverage(CurrentCvgAmnt, ElectedCvgAmnt);
	healthQuestions2PageAction=healthQuestions1PageAction.validatePreApprovedQuestionSelection1();
	healthQuestions3PageAction=healthQuestions2PageAction.validatePreApprovedQuestions2();
	reviewAndSignPageAction=healthQuestions3PageAction.validateReferredMultipleQuestion(AddtionalDetail,NameMedication,DateOfUse,DatePrescribed,NameAddressOfPrescribed);
	demoDocumentSignPageAction=reviewAndSignPageAction.validateReviewPage();
	confirmationPageAction=demoDocumentSignPageAction.validateSign();
	homePageAction=confirmationPageAction.validateReferredIndividualQuestionConfirmation();
	homePageAction.validateReferredConfirmationStatus();
	homePageAction.logOut();

	
}}
