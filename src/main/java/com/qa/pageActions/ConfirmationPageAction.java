package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import com.qa.pages.ConfirmationPage;
import com.qa.util.GenericFunction;

public class ConfirmationPageAction extends ConfirmationPage{
	public ConfirmationPageAction() {
		super();
		}
	
	/* *****************************************************************************
	  * Test Name: Confirmation Page Action 
	  * Purpose : This class contains the action of  the Confirmation Page
	  * History : Created by Anjali Johny on 07/19/2021 
  ***********************************************************************************/
	public HomePageAction validateConfirmation() throws Exception {
		Thread.sleep(5000);
		assertElementDisplayed(headerConfirmation,"Confirmation Page Header");
		takeScreenshot("ConfirmationPage");
		assertText(txtPreApproved,"Pre-Approved","Decision Complete Status");
		Thread.sleep(1000);
		assertElementDisplayed(btnMySummary,"My Summary button");
		//assertTrue(btnMySummary.isDisplayed());
		ClickElement(btnMySummary, "My Summary Button");
		return new HomePageAction();
	}

	public HomePageAction validateReferredIndividualQuestionConfirmation() throws Exception {
		Thread.sleep(5000);
		assertElementDisplayed(headerConfirmation,"Confirmation Page Header");
		takeScreenshot("ConfirmationPage");
		assertText(txtReferred,"Referred","Decision Complete Status");
		Thread.sleep(1000);
	    assertElementDisplayed(btnMySummary,"My Summary button");
		//assertTrue(btnMySummary.isDisplayed());
		ClickElement(btnMySummary, "My Summary Button");
		return new HomePageAction();
	}
	
	public HomePageAction validateDeclinedConfirmation() throws Exception {
		Thread.sleep(5000);
		assertElementDisplayed(headerConfirmation,"Confirmation Page Header");
		takeScreenshot("ConfirmationPage");
		assertText(txtDeclined,"Declined","Decision Complete Status");
		Thread.sleep(1000);
		assertElementDisplayed(btnMySummary,"My Summary button");
		//assertTrue(btnMySummary.isDisplayed());
		ClickElement(btnMySummary, "My Summary Button");
		return new HomePageAction();
	}
	
//	10/28 - Added by Pallavi for Emp Only SSO user Approved scenario
	public HomePageAction validateApprovedConfirmationSSO() throws InterruptedException {
		Thread.sleep(5000);
		assertElementDisplayed(headerConfirmation,"Confirmation Page Header");
		takeScreenshot("ConfirmationPage");
		assertText(txtApprovedSSO,"Approved","Decision Complete Status");
		Thread.sleep(1000);
		assertElementDisplayed(btnMySummary,"My Summary button");
		ClickElement(btnMySummary, "My Summary Button");
		return new HomePageAction();
	}
	
//	12/7/21 - Added by Pallavi for Spouse only SSO user Referred scenario
	public HomePageAction validateReferredConfirmationSSOSpouseOnly() throws InterruptedException {
		Thread.sleep(5000);
		assertElementDisplayed(headerConfirmation,"Confirmation Page Header");
		takeScreenshot("ConfirmationPage");
		assertText(txtReferredSSOSpouseOnly, "Referred", "Decision Complete Status");
		Thread.sleep(1000);
		assertElementDisplayed(btnMySummary,"My Summary button");
		ClickElement(btnMySummary, "My Summary Button");
		return new HomePageAction();
		
	}
	
}