package com.qa.pageActions;



import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;

import com.qa.pages.LoginPage;
import com.qa.pages.UpdateSecurityInformationPage;

public class UpdateSecurityInformationPageAction extends UpdateSecurityInformationPage {
	public UpdateSecurityInformationPageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : UpdateSecurityInformationPageAction 
	  * Purpose : This class contains the action of  the UpdateSecrityInformation page
	  * * History : Created by Anjali Johny on 07/02/2021 
	  **************************************************************************************/
	public static void pressTABKey() {
		try {
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.delay(100);
			r.keyRelease(KeyEvent.VK_TAB);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
	
	public HomePageAction validateSecurityInformation(String validTempPwd,String LessThan8Chrs,String MoreThan20Chrs,String AlphanumericandCaseChrs,String PwdwithInvalidSpclChr,
			String PwdwithUserID,String PwdwithSpace,String validPwd,String invalidConfirmPwd,String validConfirmPwd) throws Exception {

		
		String SecInfoPageHeader = headerUpdateSecInfo.getText();
		takeScreenshot("UpdateSecurityInformationPage1");	
		assertText(headerUpdateSecInfo,"Update Your Security Information","Update Your Security Information");
		//Assert.assertEquals(SecInfoPageHeader, "Update Your Security Information");
		validateTempPassword(validTempPwd);
		validateNewPassword(LessThan8Chrs,MoreThan20Chrs,AlphanumericandCaseChrs,PwdwithInvalidSpclChr,
				PwdwithUserID,PwdwithSpace,validPwd);
		validateConfirmPassword(invalidConfirmPwd, validConfirmPwd);
		takeScreenshot("UpdateSecurityInformationPage2");
		ClickElement(btnSubmit,"Submit Button");
		return new HomePageAction();	
		
	}
	
	//Validating New Password field
	public void validateNewPassword(String LessThan8Chrs,String MoreThan20Chrs,String AlphanumericandCaseChrs,String PwdwithInvalidSpclChr,
			String PwdwithUserID,String PwdwithSpace,String validPwd) throws Exception {
		validatingPassword_LessThn8Chrs(LessThan8Chrs);
		validatingPassword_MoreThn20Chrs(MoreThan20Chrs);
		validatingPassword_WithCaseandAlphanumericChrs(AlphanumericandCaseChrs);
		validatingPassword_WithInvalidSpclChrs(PwdwithInvalidSpclChr);
		validatingPassword_ContainsUserID(PwdwithUserID);
		validatingPassword_PwdContainsSpace(PwdwithSpace);
		validatingPassword_withValidPwd(validPwd);
	}
	
	//Validate Confirm Password field with different inputs
	public void validateConfirmPassword(String invalidConfirmPwd, String validConfirmPwd) {
		validatingConfirmPwd_WithInvalidConfirmPwd(invalidConfirmPwd);
		validatingConfirmPassword_WithValidConfirmPwd(validConfirmPwd);
	}
	
	// Validate Temporary Password text field 
			public void validateTempPassword(String validTempPwd ) {
				EnterText(txtTempPassword,validTempPwd,"Temporary Password");
				takeScreenshot("UpdateSecurityInformationPage_TempPassword");
		}
			//Validating Password field based on different criteria's
			public void validatingPassword_LessThn8Chrs(String LessThan8Chrs) {
				EnterText(txtNewPassword, LessThan8Chrs, "Password with less than 8 chrs");
				pressTABKey();
				try {
					if (msgPassword_Validation.isDisplayed()) {
						String PwdValidationMsg = msgPassword_Validation.getText();
						
						assertText(msgPassword_Validation, "Invalid password. Please check formatting rules and try again.", "Invalid password error message");
						//Assert.assertEquals(PwdValidationMsg,
							//	"Invalid password. Please check formatting rules and try again.");
						takeScreenshot("UpdateSecurityInfo_NewPassword_Lessthan8");
						try {
							if (msgMin8Characters_Validation.isDisplayed()) {
								String PwdlessThan8chrs = msgMin8Characters_Validation.getText();
								System.out.println("Validation message displayed: "+PwdlessThan8chrs);
							}
						} catch (NoSuchElementException e) {
							
							System.out.println("'8 characters minimum' validation message is not displaying");
						}
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_NewPassword_Lessthan8");
					System.out.println("Password validation message is not displaying");
				}
			}
			public void validatingPassword_MoreThn20Chrs(String MoreThan20Chrs) throws Exception {
				EnterText(txtNewPassword, MoreThan20Chrs, "Password with more than 20 chrs");
				pressTABKey();
				try {
					if (msgPassword_Validation.isDisplayed()) {
						String PwdValidationMsg = msgPassword_Validation.getText();
						
						assertText(msgPassword_Validation, "Invalid password. Please check formatting rules and try again.", "Invalid password error message");
						//Assert.assertEquals(PwdValidationMsg,
								//"Invalid password. Please check formatting rules and try again.");
						takeScreenshot("UpdateSecurityInfo_NewPassword_Morethan20");
						try {
							if (msgMax20Characters_Validation.isDisplayed()) {
								String PwdMoreThan20chrs = msgMax20Characters_Validation.getText();
								System.out.println("Validation message displayed: "+PwdMoreThan20chrs);
							}
						} catch (NoSuchElementException e) {
							System.out.println("'20 characters maximum' validation message is not displaying");
						}
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_NewPassword_Morethan20");
					System.out.println("Password validation message is not displaying");
				}
			}
			
			public void validatingPassword_WithCaseandAlphanumericChrs(String AlphanumericandCaseChrs) {
				EnterText(txtNewPassword, AlphanumericandCaseChrs, "Password with Case&Alphanumeric Characters");
				pressTABKey();
				try {
					if (msgPassword_Validation.isDisplayed()) {
						String PwdValidationMsg = msgPassword_Validation.getText();
						
						assertText(msgPassword_Validation,"Invalid password. Please check formatting rules and try again.", "Invalid password error message");
					//	Assert.assertEquals(PwdValidationMsg,
							//	"Invalid password. Please check formatting rules and try again.");
						takeScreenshot("UpdateSecurityInfo_NewPassword_CaseandAlphanumeric");
						try {
							if (msgPwdCaseAndAlphnumeric_Validation.isDisplayed()) {
								String CaseandAlphanumericchrs = msgPwdCaseAndAlphnumeric_Validation.getText();
								System.out.println("Validation message displayed: "+CaseandAlphanumericchrs);
							}
						} catch (NoSuchElementException e) {
							System.out.println("'Must contain at least 3 of the following:' validation message is not displaying");
						}
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_NewPassword_CaseandAlphanumeric");
					System.out.println("Password validation message is not displaying");
				}
			}
			
			public void validatingPassword_WithInvalidSpclChrs(String PwdwithInvalidSpclChr) {
				EnterText(txtNewPassword, PwdwithInvalidSpclChr, "Password with invalid special Characters");
				pressTABKey();
				try {
					if (msgPassword_Validation.isDisplayed()) {
						String PwdValidationMsg = msgPassword_Validation.getText();
						
						assertText(msgPassword_Validation,"Invalid password. Please check formatting rules and try again.", "Invalid password error message");
						//Assert.assertEquals(PwdValidationMsg,
							//	"Invalid password. Please check formatting rules and try again.");
						takeScreenshot("UpdateSecurityInfo_NewPassword_InvalidSpecialCharacters");
						try {
							if (msgPwdSpecialCharacters_Validation.isDisplayed()) {
								String invalidSpecialchar = msgPwdSpecialCharacters_Validation.getText();
								System.out.println("Validation message displayed: "+invalidSpecialchar);
							}
						} catch (NoSuchElementException e) {
							System.out.println("'Allowable special characters include: ~ ! @ # % ^ & () - _ + =' validation message is not displaying");
						}
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_NewPassword_InvalidSpecialCharacters");
					System.out.println("Password validation message is not displaying");
				}
			}
			
			public void validatingPassword_ContainsUserID(String PwdwithUserID) {
				EnterText(txtNewPassword, PwdwithUserID, "Password contains USerID");
				pressTABKey();
				try {
					if (msgPassword_Validation.isDisplayed()) {
						String PwdValidationMsg = msgPassword_Validation.getText();
						
						assertText(msgPassword_Validation, "Invalid password. Please check formatting rules and try again.", "Invalid password error message");
						//Assert.assertEquals(PwdValidationMsg,
						//		"Invalid password. Please check formatting rules and try again.");
						takeScreenshot("UpdateSecurityInfo_NewPassword_WithUserID");
						try {
							if (msgPwdContainsUserID_Validation.isDisplayed()) {
								String PwdContainsUserID = msgPwdContainsUserID_Validation.getText();
								System.out.println("Validation message displayed: "+PwdContainsUserID);
							}
						} catch (NoSuchElementException e) {
							System.out.println("'Cannot contain User ID' validation message is not displaying");
						}
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_NewPassword_WithUserID");
					System.out.println("Password validation message is not displaying");
				}
			}
			public void validatingPassword_PwdContainsSpace(String PwdwithSpace) {
				EnterText(txtNewPassword, PwdwithSpace, "Password contains Space");
				pressTABKey();
				try {
					if (msgPassword_Validation.isDisplayed()) {
						String PwdValidationMsg = msgPassword_Validation.getText();
					
						assertText(msgPassword_Validation, "Invalid password. Please check formatting rules and try again.", "Invalid password error message");
						//Assert.assertEquals(PwdValidationMsg,
							//	"Invalid password. Please check formatting rules and try again.");
						takeScreenshot("UpdateSecurityInfo_NewPassword_WithSpace");
						try {
							if (msgPwdContainsSpace_Validation.isDisplayed()) {
								String PwdContainsSpace = msgPwdContainsSpace_Validation.getText();
								System.out.println("Validation message displayed: "+PwdContainsSpace);
							}
						} catch (NoSuchElementException e) {
							takeScreenshot("UpdateSecurityInfo_NewPassword_WithSpace");
							System.out.println("'Cannot contain spaces' validation message is not displaying");
						}
					}
				} catch (NoSuchElementException e) {
					System.out.println("Password validation message is not displaying");
				}
			}
			
			public void validatingPassword_withValidPwd(String validPwd) {
				EnterText(txtNewPassword, validPwd, "Password Valid data");
				pressTABKey();
				try {
					if (msgPassword_Validation.isDisplayed()) {
						takeScreenshot("UpdateSecurityInfo_NewPassword_Valid");
						System.out.println("Password validation message is displaying and feature is not working as expected");
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_NewPassword_Valid");
					System.out.println("Password validation message is not displaying and feature is working as expected");
				}
			}
			
			
			//Validating Confirm Password field with valid and invalid data
			public void validatingConfirmPwd_WithInvalidConfirmPwd(String invalidConfirmPwd) {
				EnterText(txtConfirmPassword, invalidConfirmPwd, "Confirm Password with invalid data");
				pressTABKey();
				try {
					if (msgPasswordNotMatch_Validation.isDisplayed()) {
						takeScreenshot("UpdateSecurityInfo_ConfirmPassword_InValid");
						String PwdNotMatching = msgPasswordNotMatch_Validation.getText();
						System.out.println("Validation message displayed: "+PwdNotMatching);
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_ConfirmPassword_InValid");
					System.out.println("'Passwords do not match.' validation message is not displaying");
				}
			}
			
			public void validatingConfirmPassword_WithValidConfirmPwd(String validConfirmPwd) {
				EnterText(txtConfirmPassword, validConfirmPwd, "Confirm Password with valid data");
				pressTABKey();
				try {
					if (msgPasswordNotMatch_Validation.isDisplayed()) {
						takeScreenshot("UpdateSecurityInfo_ConfirmPassword_Valid");
						System.out.println("Confirm Password validation message is displaying and feature is not working as expected");
					}
				} catch (NoSuchElementException e) {
					takeScreenshot("UpdateSecurityInfo_ConfirmPassword_Valid");
					System.out.println("Confirm Password validation message is not displaying and feature is working as expected");
				}
			}
}
		