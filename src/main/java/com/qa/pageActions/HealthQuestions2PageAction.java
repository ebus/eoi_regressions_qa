package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import com.qa.pages.HealthQuestions2Page;

public class HealthQuestions2PageAction extends HealthQuestions2Page{
	public HealthQuestions2PageAction(){
		super();
	}
	/* *****************************************************************************
	  * Test Name : HealthQuestions2 PageAction 
	  * Purpose : This class contains the action of  the HealthQuestions2 page
	  * History : Created by Anjali Johny on 07/19/2021 
	  **************************************************************************************/
	public HealthQuestions3PageAction validatePreApprovedQuestions2() throws Exception {
		Thread.sleep(2000);
		assertElementDisplayed(headerHealthQuestion2a,"Header of Step 3");
		takeScreenshot("HealthQuestion2Up_Step3");
		ClickElement(radiobtnQuestion3a_NO,"Question3a_No Radio Button");
		ClickElement(radiobtnQuestion3b_NO,"Question3b_No Radio Button");
		ClickElement(radiobtnQuestion3c_NO,"Question3c_No Radio Button");
		ClickElement(radiobtnQuestion3d_NO,"Question3d_No Radio Button");
		ClickElement(radiobtnQuestion3e_NO,"Question3e_No Radio Button");
		ClickElement(radiobtnQuestion3f_NO,"Question3f_No Radio Button");
		ClickElement(radiobtnQuestion3g_NO,"Question3g_No Radio Button");
		ClickElement(radiobtnQuestion3h_NO,"Question3h_No Radio Button");
		ClickElement(radiobtnQuestion3i_NO,"Question3i_No Radio Button");
		ClickElement(radiobtnQuestion3j_NO,"Question3j_No Radio Button");
		ClickElement(radiobtnQuestion3k_NO,"Question3k_No Radio Button");
		ClickElement(radiobtnQuestion3l_NO,"Question3l_No Radio Button");
		ClickElement(radiobtnQuestion3m_NO,"Question3m_No Radio Button");
		takeScreenshot("HealthQuestion2Down_Step3");
		ClickElement(radiobtnQuestion3n_NO, "Question3n_No Radio Button");
		assertElementDisplayed(btnBack,"Back button");
		assertElementDisplayed(btnSaveFinish,"Save & Finish button");
		assertElementDisplayed(btnSaveContinue,"Save & Continue button");
		//assertTrue(btnBack.isDisplayed());
		//assertTrue(btnSaveFinish.isDisplayed());
		//assertTrue(btnSaveContinue.isDisplayed());
		takeScreenshot("HealthQuestion1Buttons_Step3");
		scrollPageDown(driver);
		Thread.sleep(2000);
		ClickElement(btnSaveContinue,"Save and Continue Button");
		Thread.sleep(1000);
		return new HealthQuestions3PageAction();
}}