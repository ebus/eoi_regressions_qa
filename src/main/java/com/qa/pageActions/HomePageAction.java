package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import org.testng.Assert;   //Added by Pallavi on 11/08

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;

//import junit.framework.Assert;

public class HomePageAction extends HomePage {
	public HomePageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name :HomePageAction 
	  * Purpose : This class contains the action of  the Home page
	  * History : Created by Anjali Johny on 07/05/2021 
   ***********************************************************************************/
	public PersonalInformationPageAction validateHomepage() throws InterruptedException {
	Thread.sleep(2000);
		//assertTrue(linkHomepage.isDisplayed());
		assertElementDisplayed(linkHomepage,"Home page");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		assertElementDisplayed(btnBeginEOI,"Begin EOI button");
	
		//assertTrue(btnBeginEOI.isDisplayed());
		takeScreenshot("HomePage");
		assertText(btnBeginEOI, "Begin EOI","Begin EOI Button");
		scrollIntoView(headerHomePage, driver);
		takeScreenshot("HomePage_EOI_button");
		ClickElement(btnBeginEOI, "Begin EOI button");  // Added by Pallavi on 10/27
//		btnBeginEOI.click();
//		btnContinueEOI.click();
		Thread.sleep(2000);
		return new PersonalInformationPageAction();
	}
	//for scripting rerun purpose only this method is created
	public HealthQuestions1PageAction validatedemo() {
		btnContinueEOI.click();
		return new HealthQuestions1PageAction();
	
	}
	public void validatePreApprovedConfirmationStatus() throws Exception {
		//assertTrue(linkHomepage.isDisplayed());
		assertElementDisplayed(linkHomepage,"Home page");
		Thread.sleep(2000);
		takeScreenshot("MySummaryPage_PreApproved_ConfirmationStatus_Up");
		scrollIntoView(lblViewConfirmation, driver);
		takeScreenshot("MySummaryPage_PreApproved_ConfirmationStatus_Down");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		assertText(lblPreApprovedStatus,"Pre-Approved","Status Label");
		//return new ConfirmationPageAction();
	}
	public void validateReferredConfirmationStatus() throws Exception {
		//assertTrue(linkHomepage.isDisplayed());
		assertElementDisplayed(linkHomepage,"Home page");
		Thread.sleep(2000);
	//	scrollIntoView(lblEOIStatus, driver);
		takeScreenshot("MySummaryPage_Referred_ConfirmationStatus_Up");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		scrollIntoView(lblViewConfirmation, driver);
		takeScreenshot("MySummaryPage_Referred_ConfirmationStatus_Down");
		assertText(lblReferredStatus,"Referred","Status Label");
		//return new ConfirmationPageAction();
	}
	public void validateDeclinedConfirmationStatus() throws Exception {
		//assertTrue(linkHomepage.isDisplayed());
		assertElementDisplayed(linkHomepage,"Home page");
		Thread.sleep(2000);	
		takeScreenshot("MySummaryPage_Declined_onfirmationStatus_Up");
		scrollIntoView(lblViewConfirmation, driver);
		takeScreenshot("MySummaryPage_DeclinedConfirmationStatus_Down");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		assertText(lblDeclinedStatus,"Declined","Status Label");
		//return new ConfirmationPageAction();
	}
	
//	10/28 - Added by Pallavi for SSO user Approved scenario
	public void validateApprovedConfirmationStatusSSO() throws InterruptedException {
		Thread.sleep(3000);
		assertElementDisplayed(linkHomepage,"Home page");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		Thread.sleep(3000);
		takeScreenshot("HomePage_Approved_ConfirmationStatus_Up");
		scrollIntoView(lblViewConfirmation, driver);
		Thread.sleep(3000);
		takeScreenshot("HomePage_Approved_ConfirmationStatus_Down");
		assertText(listLblApprovedSSO.get(0),"Approved","Status Label");
		Thread.sleep(3000);
	}
	
//	11/4 - Added by Pallavi for scripting/debugging purpose
	public PersonalInformationPageAction validateHomePageContEOI() throws InterruptedException {
//	public CoverageChangeRequestPageAction validateHomePageContEOI() throws InterruptedException {
//	public ReviewAndSignPageAction validateHomePageContEOI() throws InterruptedException {
		assertElementDisplayed(linkHomepage,"Home page");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		assertElementDisplayed(btnContinueEOI,"Continue EOI button");
		scrollIntoView(headerHomePage, driver);
		takeScreenshot("HomePage_EOI_button");
		ClickElement(btnContinueEOI, "Continue EOI button is clicked");
		Thread.sleep(3000);
		return new PersonalInformationPageAction();
//		return new CoverageChangeRequestPageAction();
//		return new ReviewAndSignPageAction();
	}
	
//	12/7/21 - Added by Pallavi for Spouse only SSO user Referred scenario
	public void validateReferredConfStatusSpouseOnlySSO() throws InterruptedException {
		Thread.sleep(3000);
		assertElementDisplayed(linkHomepage,"Home page");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		takeScreenshot("HomePage_Referred_ConfirmationStatus_Up");
		Thread.sleep(3000);
		scrollIntoView(lblViewConfirmation, driver);
		Thread.sleep(3000);
		takeScreenshot("HomePage_Referred_ConfirmationStatus_Down");
//		System.out.println("Status is: "+ listLblReferredSSO.get(0).getText());
		assertText(listLblReferredSSO.get(0), "Referred", "Status Label");
		Thread.sleep(3000);
	}
	
//	12/24/21 - Added by Pallavi for Spouse Only SSO user Declined scenario
	public void validateDeclinedConfStatusSpouseOnlySSO() throws InterruptedException {
		Thread.sleep(3000);
		assertElementDisplayed(linkHomepage,"Home page");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		takeScreenshot("HomePage_Declined_ConfirmationStatus_Up");
		Thread.sleep(3000);
		scrollIntoView(lblViewConfirmation, driver);
		Thread.sleep(3000);
		takeScreenshot("HomePage_Declined_ConfirmationStatus_Down");
//		System.out.println("Status is: "+ listLblDeclinedSSO.get(0).getText());
		assertText(listLblDeclinedSSO.get(0), "Declined", "Status Label");
		Thread.sleep(3000);
	}
	
	
	public void logOut() {
		ClickElement(linkLogOut,"LogOut link");
	}
	
}