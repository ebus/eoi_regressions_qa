package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import com.qa.pages.DemoDocumentSignPage;

public class DemoDocumentSignPageAction extends DemoDocumentSignPage{
	public DemoDocumentSignPageAction(){
		super();
	}
	/* *****************************************************************************
	  * Test Name :Docu Sign Page Action 
	  * Purpose : This class contains the action of  the Docu Sign Page
	  * History : Created by Anjali Johny on 07/20/2021 
 ***********************************************************************************/
public ConfirmationPageAction validateSign() throws Exception {
	Thread.sleep(3000);
	takeScreenshot("DemoDocumentSign_Up");
	ClickElement(cbDiclosureAccept,"Disclosure Accept Checkbox");
	Thread.sleep(1000);
	takeScreenshot("DemoDocumentSign_Check box");
	ClickElement(btnContinue,"Continue Button");
	ClickElement(btnStart,"Start Button");
	Thread.sleep(3000);
	takeScreenshot("DemoDocumentSign_Down");
	ClickElement(btnSignIcon,"Sign Icon");
	Thread.sleep(2000);
	takeScreenshot("DemoDocumentSign_AdoptSign1");
    assertElementDisplayed(headerAdoptSign,"Adopt Sign Header");
    assertElementDisplayed(btnAdoptSign,"Adopt Sign button");
    assertElementDisplayed(btnCancel,"Cancel button");
    // assertTrue(btnAdoptSign.isDisplayed());
    //assertTrue(btnCancel.isDisplayed());
    takeScreenshot("DemoDocumentSign_AdoptSign2");
    ClickElement(btnAdoptSign,"Adopt Sign Button");
    Thread.sleep(2000);
    takeScreenshot("DemoDocumentSign_Finish");
    ClickElement(btnFinish,"Finish Button");
    Thread.sleep(5000);
	return new ConfirmationPageAction();
	}
}