package com.qa.pageActions;


import static org.testng.Assert.assertTrue;

import com.qa.pages.CoverageChangeRequestPage;
import com.relevantcodes.extentreports.LogStatus;

public class CoverageChangeRequestPageAction extends CoverageChangeRequestPage {
	public CoverageChangeRequestPageAction() {
		super();
	}
	
	/* *****************************************************************************
	  * Test Name : CoverageChangeRequestPageAction 
	  * Purpose : This class contains the action of CoverageChangeRequest page
	  * History : Created by Anjali Johny on 07/17/2021 
	  **************************************************************************************/
	public HealthQuestions1PageAction validateCoverage(String CurrentCoverage,String ElectedCoverage) throws InterruptedException {
	assertText(headerStep2,"Coverage or Change Being Requested","Step 2");
	assertElementDisplayed(tblEmpcoverage,"Employe Coverage table");
	//assertTrue(tblEmpcoverage.isDisplayed());
	takeScreenshot("CoverageChangeRequest_Up");
	assertText(txtRow1_Benefits, "Benefit","Column 1");
	assertText(txtRow2_Class, "Class (Optional)","Column 2");
	assertText(txtRow3_CurrentCoverage, "Current Coverage", "Column 3");
	assertText(txtRow4_ElectedCoverage, "Elected Coverage","Column 4");
	EnterText(txtCoverageAmount,CurrentCoverage,"Current Coverage Amount");
	EnterText(txtElectedAmount,ElectedCoverage,"Elected Coverage Amount");
	takeScreenshot("CoverageChangeRequest_Down");
	assertElementDisplayed(btnBack,"Back button");
	assertElementDisplayed(btnSaveFinish,"Save & Finish button");
	assertElementDisplayed(btnSaveContinue,"Save & Continue button");
	
	//assertTrue(btnBack.isDisplayed());
	//assertTrue(btnSaveFinish.isDisplayed());
	//assertTrue(btnSaveContinue.isDisplayed());
	ClickElement(btnSaveContinue,"Save and Continue Button");
	Thread.sleep(3000);
	return new HealthQuestions1PageAction();
	}
	
//	10/28 - Added by Pallavi for SSO user scenarios
	public HealthQuestions1PageAction verifyCoverageSSO() throws InterruptedException {
		assertText(headerStep2,"Coverage or Change Being Requested","Step 2");
		assertElementDisplayed(headerMyCoverageDetails,"Spouse/Domestic Partner Coverage");
		takeScreenshot("CoverageChangeRequestPage_Up");
		scrollPageDown(driver);
		takeScreenshot("CoverageChangeRequestPage_Down");
		assertElementDisplayed(btnBack,"Back button");
		assertElementDisplayed(btnSaveFinish,"Save & Finish button");
		assertElementDisplayed(btnSaveContinue,"Save & Continue button");
		ClickElement(btnSaveContinue,"Save & Continue button");
		Thread.sleep(3000);
		return new HealthQuestions1PageAction();
	}

}