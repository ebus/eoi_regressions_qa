package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import com.qa.pages.HealthQuestions3Page;

public class HealthQuestions3PageAction extends HealthQuestions3Page{
	public HealthQuestions3PageAction(){
		super();
	}
	/* *****************************************************************************
	  * Test Name : HealthQuestions3 PageAction 
	  * Purpose : This class contains the action of  the HealthQuestions3 page
	  * History : Created by Anjali Johny on 07/19/2021 
	  **************************************************************************************/
	public ReviewAndSignPageAction validatePreApprovedQuestions3() throws Exception {
		Thread.sleep(2000);
		assertElementDisplayed(headerHealthQuestions3,"Header of Step 3");
		takeScreenshot("HealthQuestion3Up_Step3");
		ClickElement(radiobtnQuestion4_NO,"Question4_No Radio Button");
		ClickElement(radiobtnQuestion5_NO,"Question5_No Radio Button");
		ClickElement(radiobtnQuestion6_NO,"Question6_No Radio Button");
		ClickElement(radiobtnQuestion7_NO,"Question7_No Radio Button");
		ClickElement(radiobtnQuestion8_NO,"Question8_No Radio Button");
		takeScreenshot("HealthQuestion3Down_Step3");
		ClickElement(radiobtnQuestion9_NO,"Question9_No Radio Button");
		ClickElement(radiobtnQuestion10_NO,"Question10_No Radio Button");
		assertElementDisplayed(btnBack,"Back button");
		assertElementDisplayed(btnSaveFinish,"Save & Finish button");
		assertElementDisplayed(btnSaveContinue,"Save & Continue button");
		//assertTrue(btnBack.isDisplayed());
		//assertTrue(btnSaveFinish.isDisplayed());
		//assertTrue(btnSaveContinue.isDisplayed());
		takeScreenshot("HealthQuestion3Buttons_Step3");
		scrollPageDown(driver);
		Thread.sleep(2000);
		ClickElement(btnSaveContinue,"Save and Continue Button");
		Thread.sleep(1000);
		return new ReviewAndSignPageAction();
}
	public ReviewAndSignPageAction validateReferredIndividualQuestions3(String Question,String AddtionalDetail,String NameMedication,String DateOfUse,String DatePrescribed,String NameAddressOfPrescribed) throws Exception {
		Thread.sleep(1000);
		assertElementDisplayed(headerHealthQuestions3,"Header of Step 3");
		takeScreenshot("HealthQuestion3Up_Step3");

		//Question 4
		if(Question.equalsIgnoreCase("4")) {
			ClickElement(radiobtnQuestion4_Yes,"Question4_Yes Radio Button");
			validateAdditionalTextQuestion4(NameMedication,DateOfUse,DatePrescribed,NameAddressOfPrescribed);
		}
		else
		{
			ClickElement(radiobtnQuestion4_NO,"Question4_No Radio Button");
		}
		//Question 5
		
		if(Question.equalsIgnoreCase("5")) {
			ClickElement(radiobtnQuestion5_Yes,"Question5_Yes Radio Button");
			validateAdditionalQuestion(AddtionalDetail);
		}
		else
		{
			ClickElement(radiobtnQuestion5_NO,"Question5_No Radio Button");
		}
		
		//Question 6
		
		if(Question.equalsIgnoreCase("6")) {
			ClickElement(radiobtnQuestion6_Yes,"Question6_Yes Radio Button");
			validateAdditionalQuestion(AddtionalDetail);
		}
		else
		{
			ClickElement(radiobtnQuestion6_NO,"Question6_No Radio Button");
		}
		
		//Question 7
		
		if(Question.equalsIgnoreCase("7")) {
			ClickElement(radiobtnQuestion7_Yes,"Question7_Yes Radio Button");
			validateAdditionalQuestion(AddtionalDetail);
		}
		else
		{
			ClickElement(radiobtnQuestion7_NO,"Question7_No Radio Button");
		}
		
		//Question 8
		takeScreenshot("HealthQuestion3Down_Step3");

		if(Question.equalsIgnoreCase("8")) {
			ClickElement(radiobtnQuestion8_Yes,"Question8_Yes Radio Button");
			validateAdditionalQuestion(AddtionalDetail);
		}
		else
		{
			ClickElement(radiobtnQuestion8_NO,"Question8_No Radio Button");
		}
		
      
			ClickElement(radiobtnQuestion9_NO,"Question9_No Radio Button");
			ClickElement(radiobtnQuestion10_NO,"Question10_No Radio Button");
		
				
		assertTrue(btnBack.isDisplayed());
		assertTrue(btnSaveFinish.isDisplayed());
		assertTrue(btnSaveContinue.isDisplayed());
		takeScreenshot("HealthQuestion3Buttons_Step3");
		scrollPageDown(driver);
		Thread.sleep(2000);
		ClickElement(btnSaveContinue,"Save and Continue Button");
		Thread.sleep(1000);
		return new ReviewAndSignPageAction();
}	
	public ReviewAndSignPageAction validateReferredMultipleQuestion(String AddtionalDetail,String NameMedication,String DateOfUse,String DatePrescribed,String NameAddressOfPrescribed ) throws Exception {
		Thread.sleep(1000);
		assertElementDisplayed(headerHealthQuestions3,"Header of Step 3");
		takeScreenshot("HealthQuestion3Up_Step3");
		//Question 4
		ClickElement(radiobtnQuestion4_Yes,"Question4_Yes Radio Button");
		validateAdditionalTextQuestion4(NameMedication,DateOfUse,DatePrescribed,NameAddressOfPrescribed);
		//Question 5
		ClickElement(radiobtnQuestion5_Yes,"Question5_Yes Radio Button");
		validateAdditionalQuestion5(AddtionalDetail);
		//Question 6
		ClickElement(radiobtnQuestion6_Yes,"Question6_Yes Radio Button");
		validateAdditionalQuestion6(AddtionalDetail);
		//Question 7
		ClickElement(radiobtnQuestion7_Yes,"Question7_Yes Radio Button");
			validateAdditionalQuestion7(AddtionalDetail);
		//Question 8
		takeScreenshot("HealthQuestion3Down_Step3");
		ClickElement(radiobtnQuestion8_Yes,"Question8_Yes Radio Button");
		validateAdditionalQuestion8(AddtionalDetail);
		ClickElement(radiobtnQuestion9_NO,"Question9_No Radio Button");
		ClickElement(radiobtnQuestion10_NO,"Question10_No Radio Button");
		assertTrue(btnBack.isDisplayed());
		assertTrue(btnSaveFinish.isDisplayed());
		assertTrue(btnSaveContinue.isDisplayed());
		takeScreenshot("HealthQuestion3Buttons_Step3");
		scrollPageDown(driver);
		Thread.sleep(2000);
		ClickElement(btnSaveContinue,"Save and Continue Button");
		Thread.sleep(1000);
		return new ReviewAndSignPageAction();
	}
	
	public void validateAdditionalTextQuestion4(String NameMedication,String DateOfUse,String DatePrescribed,String NameAddressOfPrescribed) {
      EnterText(txtMedication,NameMedication,"Name of Medication");
      EnterText(txtDate,DateOfUse,"Date(s) in Use");
      EnterText(txtDatePrescribed,DatePrescribed,"Date Prescribed");
      EnterText(txtNameAddr,NameAddressOfPrescribed,"Name and Address of Prescriber");
      takeScreenshot("Question4_Yes");
	}
	public void validateAdditionalQuestion5(String AddtionalDetail) {
		assertTrue(lblAdditionalDetailQ5.isDisplayed());
		EnterText(txtAdditionalDetailsQuestion5,AddtionalDetail,"Additional Detail");
		takeScreenshot("Question5_Yes");
	}
	public void validateAdditionalQuestion6(String AddtionalDetail) {
		assertTrue(lblAdditionalDetailQ6.isDisplayed());
		EnterText(txtAdditionalDetailsQuestion6,AddtionalDetail,"Additional Detail");
		takeScreenshot("Question6_Yes");
	}
	public void validateAdditionalQuestion7(String AddtionalDetail) {
		assertTrue(lblAdditionalDetailQ7.isDisplayed());
		EnterText(txtAdditionalDetailsQuestion7,AddtionalDetail,"Additional Detail");
		takeScreenshot("Question7_Yes");
	}
	public void validateAdditionalQuestion8(String AddtionalDetail) {
		assertTrue(lblAdditionalDetailQ8.isDisplayed());
		EnterText(txtAdditionalDetailsQuestion8,AddtionalDetail,"Additional Detail");
		takeScreenshot("Question8_Yes");
	}
	public void validateAdditionalQuestion(String AddtionalDetail) {
		assertTrue(lblAdditionalDetailQ.isDisplayed());
		EnterText(txtAdditionalDetailsQuestion,AddtionalDetail,"Additional Detail");
		takeScreenshot("Question8_Yes");
	}
	
}