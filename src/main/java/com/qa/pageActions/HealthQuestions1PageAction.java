package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import com.qa.pages.HealthQuestions1Page;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;

public class HealthQuestions1PageAction extends HealthQuestions1Page{
	public HealthQuestions1PageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name :HomePageAction 
	  * Purpose : This class contains the action of  the HealthQuestions1 Page
	  * History : Created by Anjali Johny on 07/19/2021 
   ***********************************************************************************/
	public HealthQuestions2PageAction validatePreApprovedQuestionSelection1() throws InterruptedException {
	takeScreenshot("HealthQuestion1Up_Step3");
	assertElementDisplayed(headerHealthQuestionsStep,"Header of Step 3");
	ClickElement(radiobtnQuestion1a_NO,"Question1_No Radio Button");
	ClickElement(radiobtnQuestion2a_NO,"Question2a_No Radio Button");
	ClickElement(radiobtnQuestion2b_NO,"Question2b_No Radio Button");
	ClickElement(radiobtnQuestion2c_NO,"Question2c_No Radio Button");
	ClickElement(radiobtnQuestion2d_NO,"Question2d_No Radio Button");
	ClickElement(radiobtnQuestion2e_NO,"Question2e_No Radio Button");
	ClickElement(radiobtnQuestion2f_NO,"Question2f_No Radio Button");
	ClickElement(radiobtnQuestion2g_NO,"Question2g_No Radio Button");
	ClickElement(radiobtnQuestion2h_NO,"Question2h_No Radio Button");
	ClickElement(radiobtnQuestion2i_NO,"Question2i_No Radio Button");
	ClickElement(radiobtnQuestion2j_NO,"Question2j_No Radio Button");
	ClickElement(radiobtnQuestion2k_NO,"Question2k_No Radio Button");
	ClickElement(radiobtnQuestion2l_NO,"Question2l_No Radio Button");
	ClickElement(radiobtnQuestion2m_NO,"Question2m_No Radio Button");
	ClickElement(radiobtnQuestion2n_NO,"Question2n_No Radio Button");
	ClickElement(radiobtnQuestion2o_NO,"Question2o_No Radio Button");
	ClickElement(radiobtnQuestion2p_NO,"Question2p_No Radio Button");
	ClickElement(radiobtnQuestion2q_NO,"Question2q_No Radio Button");
	ClickElement(radiobtnQuestion2r_NO,"Question2r_No Radio Button");
	ClickElement(radiobtnQuestion2s_NO,"Question2s_No Radio Button");
	ClickElement(radiobtnQuestion2t_NO,"Question2t_No Radio Button");
	takeScreenshot("HealthQuestion1Down_Step3");
	ClickElement(radiobtnQuestion2u_NO,"Question2u_No Radio Button");
	scrollIntoView(btnSaveContinue, driver);
	Thread.sleep(2000);
	assertElementDisplayed(btnBack,"Back button");
	assertElementDisplayed(btnSaveFinish,"Save & Finish button");
	assertElementDisplayed(btnSaveContinue,"Save & Continue button");
	//assertTrue(btnBack.isDisplayed());
	//assertTrue(btnSaveFinish.isDisplayed());
	//assertTrue(btnSaveContinue.isDisplayed());
	scrollPageDown(driver);
	takeScreenshot("HealthQuestion1Button_Step3");
	Thread.sleep(2000);
	ClickElement(btnSaveContinue,"Save and Continue Button");
	Thread.sleep(2000);
	return new HealthQuestions2PageAction();
	}
	
	//method to select Question 1
	public ReviewAndSignPageAction validateDeclineStatusQuestionSelection() throws Exception {
		takeScreenshot("HealthQuestion1Up_Step3");
		assertElementDisplayed(headerHealthQuestionsStep,"Header of Step 3");
		ClickElement(radiobtnQuestion1a_Yes,"Question1_Yes Radio Button");
		Thread.sleep(2000);   //12/27/21 - Added by Pallavi
		takeScreenshot("HealthQuestion1_SelectYes");   //12/27/21 - Added by Pallavi
		ClickElement(radiobtnQuestion2a_NO,"Question2a_No Radio Button");
		ClickElement(radiobtnQuestion2b_NO,"Question2b_No Radio Button");
		ClickElement(radiobtnQuestion2c_NO,"Question2c_No Radio Button");
		ClickElement(radiobtnQuestion2d_NO,"Question2d_No Radio Button");
		ClickElement(radiobtnQuestion2e_NO,"Question2e_No Radio Button");
		ClickElement(radiobtnQuestion2f_NO,"Question2f_No Radio Button");
		ClickElement(radiobtnQuestion2g_NO,"Question2g_No Radio Button");
		ClickElement(radiobtnQuestion2h_NO,"Question2h_No Radio Button");
		ClickElement(radiobtnQuestion2i_NO,"Question2i_No Radio Button");
		ClickElement(radiobtnQuestion2j_NO,"Question2j_No Radio Button");
		ClickElement(radiobtnQuestion2k_NO,"Question2k_No Radio Button");
		ClickElement(radiobtnQuestion2l_NO,"Question2l_No Radio Button");
		ClickElement(radiobtnQuestion2m_NO,"Question2m_No Radio Button");
		ClickElement(radiobtnQuestion2n_NO,"Question2n_No Radio Button");
		ClickElement(radiobtnQuestion2o_NO,"Question2o_No Radio Button");
		ClickElement(radiobtnQuestion2p_NO,"Question2p_No Radio Button");
		ClickElement(radiobtnQuestion2q_NO,"Question2q_No Radio Button");
		ClickElement(radiobtnQuestion2r_NO,"Question2r_No Radio Button");
		ClickElement(radiobtnQuestion2s_NO,"Question2s_No Radio Button");
		ClickElement(radiobtnQuestion2t_NO,"Question2t_No Radio Button");
		takeScreenshot("HealthQuestion1Down_Step3");
		ClickElement(radiobtnQuestion2u_NO,"Question2u_No Radio Button");
		scrollIntoView(btnSaveContinue, driver);
		Thread.sleep(2000);
		assertElementDisplayed(btnBack,"Back button");
		assertElementDisplayed(btnSaveFinish,"Save & Finish button");
		assertElementDisplayed(btnSaveContinue,"Save & Continue button");
		//assertTrue(btnBack.isDisplayed());
		//assertTrue(btnSaveFinish.isDisplayed());
		//assertTrue(btnSaveContinue.isDisplayed());
		scrollPageDown(driver);
		takeScreenshot("HealthQuestion1Button_Step3");
		Thread.sleep(2000);
		ClickElement(btnSaveContinue,"Save and Continue Button");
		Thread.sleep(2000);
		return new ReviewAndSignPageAction() ;
	}
	
	
	
	
	//Method to select Question 2 for Declined status
	public HealthQuestions2PageAction validateDeclineStatusQuestion2Selection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician) throws Exception {
		takeScreenshot("HealthQuestion1Up_Step3");
		assertElementDisplayed(headerHealthQuestionsStep,"Header of Step 3");
		ClickElement(radiobtnQuestion1a_NO,"Question1_No Radio Button");
		ClickElement(radiobtnQuestion2a_Yes,"Question2a_Yes Radio Button");
		validateQuestion2aSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2b_NO,"Question2b_No Radio Button");
		ClickElement(radiobtnQuestion2c_Yes,"Question2c_Yes Radio Button");
		validateQuestion2cSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2d_Yes,"Question2d_Yes Radio Button");
		validateQuestion2dSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2e_NO,"Question2e_No Radio Button");
		ClickElement(radiobtnQuestion2f_Yes,"Question2f_Yes Radio Button");
		validateQuestion2fSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2g_NO,"Question2g_No Radio Button");
		ClickElement(radiobtnQuestion2h_Yes,"Question2h_Yes Radio Button");
		validateQuestion2hSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2i_Yes,"Question2i_Yes Radio Button");
		validateQuestion2iSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2j_Yes,"Question2j_Yes Radio Button");
		validateQuestion2jSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2k_Yes,"Question2k_Yes Radio Button");
		validateQuestion2kSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2l_NO,"Question2l_No Radio Button");
		ClickElement(radiobtnQuestion2m_Yes,"Question2m_Yes Radio Button");
		validateQuestion2mSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2n_NO,"Question2n_No Radio Button");
		ClickElement(radiobtnQuestion2o_Yes,"Question2o_Yes Radio Button");
		validateQuestion2oSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2p_NO,"Question2p_No Radio Button");
		ClickElement(radiobtnQuestion2q_Yes,"Question2q_Yes Radio Button");
		validateQuestion2qSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2r_Yes,"Question2r_Yes Radio Button");
		validateQuestion2rSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2s_Yes,"Question2s_Yes Radio Button");
		validateQuestion2sSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2t_NO,"Question2t_No Radio Button");
		takeScreenshot("HealthQuestion1Down_Step3");
		ClickElement(radiobtnQuestion2u_Yes,"Question2u_Yes Radio Button");
		validateQuestion2uSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		scrollIntoView(btnSaveContinue, driver);
		Thread.sleep(2000);
		assertElementDisplayed(btnBack,"Back button");
		assertElementDisplayed(btnSaveFinish,"Save & Finish button");
		assertElementDisplayed(btnSaveContinue,"Save & Continue button");
		//assertTrue(btnBack.isDisplayed());
		//assertTrue(btnSaveFinish.isDisplayed());
		//assertTrue(btnSaveContinue.isDisplayed());
		scrollPageDown(driver);
		takeScreenshot("HealthQuestion1Button_Step3");
		Thread.sleep(2000);
		ClickElement(btnSaveContinue,"Save and Continue Button");
		Thread.sleep(2000);
		return new HealthQuestions2PageAction() ;
	}
	
	

	//Method to Select Question 2 for Referred status
	public HealthQuestions2PageAction validateReferredStatusQuestion2Selection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician) throws Exception {
		takeScreenshot("HealthQuestion1Up_Step3");
		assertElementDisplayed(headerHealthQuestionsStep,"Header of Step 3");
		ClickElement(radiobtnQuestion1a_NO,"Question1_No Radio Button");
		ClickElement(radiobtnQuestion2a_NO,"Question2a_No Radio Button");
		ClickElement(radiobtnQuestion2b_Yes,"Question2b_Yes Radio Button");
		validateQuestion2bSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2c_NO,"Question2c_No Radio Button");		
		ClickElement(radiobtnQuestion2d_NO,"Question2d_No Radio Button");
		ClickElement(radiobtnQuestion2e_Yes,"Question2e_Yes Radio Button");
		validateQuestion2eSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2f_NO,"Question2f_No Radio Button");
		ClickElement(radiobtnQuestion2g_Yes,"Question2g_Yes Radio Button");
		validateQuestion2gSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2h_NO,"Question2h_No Radio Button");
		ClickElement(radiobtnQuestion2i_NO,"Question2i_No Radio Button");
		ClickElement(radiobtnQuestion2j_NO,"Question2j_No Radio Button");
		ClickElement(radiobtnQuestion2k_NO,"Question2k_No Radio Button");
		ClickElement(radiobtnQuestion2l_Yes,"Question2l_Yes Radio Button");
		validateQuestion2lSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2m_NO,"Question2m_No Radio Button");
		ClickElement(radiobtnQuestion2n_Yes,"Question2n_Yes Radio Button");
		validateQuestion2nSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2o_NO,"Question2o_No Radio Button");
		ClickElement(radiobtnQuestion2p_Yes,"Question2p_Yes Radio Button");
		validateQuestion2pSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		ClickElement(radiobtnQuestion2q_NO,"Question2q_No Radio Button");
		ClickElement(radiobtnQuestion2r_NO,"Question2r_No Radio Button");
		ClickElement(radiobtnQuestion2s_NO,"Question2s_No Radio Button");
		ClickElement(radiobtnQuestion2t_Yes,"Question2t_Yes Radio Button");
		validateQuestion2tSelection(treatmentDetails,lastTreatedDte,nameOfPhysician);
		takeScreenshot("HealthQuestion1Down_Step3");
		ClickElement(radiobtnQuestion2u_NO,"Question2u_No Radio Button");
		scrollIntoView(btnSaveContinue, driver);
		Thread.sleep(2000);
		assertElementDisplayed(btnBack,"Back button");
		assertElementDisplayed(btnSaveFinish,"Save & Finish button");
		assertElementDisplayed(btnSaveContinue,"Save & Continue button");
		//assertTrue(btnBack.isDisplayed());
		//assertTrue(btnSaveFinish.isDisplayed());
		//assertTrue(btnSaveContinue.isDisplayed());
		scrollPageDown(driver);
		takeScreenshot("HealthQuestion1Button_Step3");
		Thread.sleep(2000);
		ClickElement(btnSaveContinue,"Save and Continue Button");
		Thread.sleep(2000);
		return new HealthQuestions2PageAction() ;
	}
	
	//Method to add details for Question 2a-u
	public void validateQuestion2aSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception  {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2a,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2a,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2a,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2a");
	}
	public void validateQuestion2bSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception  {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2b,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2b,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2b,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2b");
	}
	public void validateQuestion2cSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2c,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2c,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2c,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2c");
	}
	
	public void validateQuestion2dSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2d,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2d,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2d,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2d");
	}

	public void validateQuestion2eSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2e,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2e,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2e,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2e");
	}
	public void validateQuestion2fSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2f,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2f,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2f,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2f");
	}
	public void validateQuestion2gSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2g,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2g,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2g,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2g");
	}
	public void validateQuestion2hSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2h,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2h,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2h,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2h");
	}
	public void validateQuestion2iSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2i,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2i,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2i,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2i");
	}
	public void validateQuestion2jSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2j,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2j,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2j,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2j");
	}
	public void validateQuestion2kSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2k,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2k,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2k,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2k");
	}
	public void validateQuestion2lSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2l,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2l,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2l,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2l");
	}
	public void validateQuestion2mSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2m,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2m,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2m,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2m");
	}
	public void validateQuestion2nSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2n,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2n,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2n,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2n");
	}
	public void validateQuestion2oSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2o,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2o,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2o,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2o");
	}
	public void validateQuestion2pSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2p,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2p,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2p,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2p");
	}
	public void validateQuestion2qSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2q,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2q,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2q,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2q");
	}
	public void validateQuestion2rSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2r,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2r,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2r,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2r");
	}
	public void validateQuestion2sSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2s,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2s,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2s,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2s");
	}
	public void validateQuestion2tSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2t,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2t,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2t,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2t");
	}
	public void validateQuestion2uSelection(String treatmentDetails,String lastTreatedDte,String nameOfPhysician)  throws Exception {
		Thread.sleep(2000);
		EnterText(txtTreatmentDetails2u,treatmentDetails,"Diagnosis and Treatment Details:");
		EnterText(txtTreatmentDate2u,lastTreatedDte,"Date Last Treated/Seen:");
		EnterText(txtTreamentDoctore2u,nameOfPhysician,"Name of Physician, Hospital, or Other Provider:");
		takeScreenshot("Question2u");
	}
	
}