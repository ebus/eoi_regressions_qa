package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.qa.pages.HomePage;
import com.qa.pages.PersonalInformationPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

public class PersonalInformationPageAction extends PersonalInformationPage{
	
		public PersonalInformationPageAction() {
			super();
		}
		/* *****************************************************************************
		  * Test Name :Personal Information Page Action 
		  * Purpose : This class contains the action of  the Personal Information page
		  * History : Created by Anjali Johny on 07/06/2021 
	   ***********************************************************************************/
		public CoverageChangeRequestPageAction validatePersonalInfo(String fname,String midName,String lname,String ssn,String homeaddr,String city,String state,String zip,
				String gender,String dob,String martialStatus,String heightFeet,String heightinch,String weight,String doh,String annualsalary,String phone,String type,
				String email ) throws Exception{
			
			assertText(headerPersonalInfo,"Personal Information","Step 1 Header");
			assertText(headerMyIdentification,"Employee Identification","Step 1 Subsection Header");
			Thread.sleep(2000);
			takeScreenshot("PersonalInformation_Up");
			//below code is for comparing the default value of firstname in page with the value			
			try
			{	
		 		if(txtFname.getAttribute("value").equalsIgnoreCase(fname))
		 		{
		 			extentTest.log(LogStatus.PASS,"First name's Text is " + fname);
		 		}
		 		else
		 		{
		 			extentTest.log(LogStatus.FAIL, "First name's Text is " +txtFname.getAttribute("value") + " but should be " + fname);
		 			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
		 		}
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL,"First name's Text is " +txtFname.getAttribute("value") + " but should be " + fname );
				extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
			}
			Thread.sleep(1000);
			//below code is for comparing the default value of midname in page with the value	
			
			if(!midName.equalsIgnoreCase("NA")) {
				if(txtMidname.getAttribute("value").equalsIgnoreCase(midName))
		 		{
		 			extentTest.log(LogStatus.PASS,"Middle name's Text is " + midName);
		 		}
		 		else
		 		{
		 			extentTest.log(LogStatus.FAIL, "Middle name's Text is " +txtMidname.getAttribute("value") + " but should be " + midName);
		 			
			}}
			else{
					System.out.println("Middle Name is Optional");
				}
				
			//below code is for comparing the default value of lastname in page with the value			
			try
			{	
		 		if(txtLastname.getAttribute("value").equalsIgnoreCase(lname))
		 		{
		 			extentTest.log(LogStatus.PASS,"Last name's Text is " + lname);
		 		}
		 		else
		 		{
		 			extentTest.log(LogStatus.FAIL, "Last name's Text is " +txtLastname.getAttribute("value") + " but should be " + lname);
		 			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
		 		}
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL,"Last name's Text is " +txtLastname.getAttribute("value") + " but should be " + lname);
				extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
			}
			
			
		
			//below code is for comparing the default value of ssn in page with the value			
			try
			{	
		 		if(txtSSN.getAttribute("value").equalsIgnoreCase(ssn))
		 		{
		 			extentTest.log(LogStatus.PASS,"SSN's Text is " +ssn);
		 		}
		 		else
		 		{
		 			extentTest.log(LogStatus.FAIL, "SSN's Text is " +txtSSN.getAttribute("value") + " but should be " + ssn);
		 			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
		 		}
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL,"SSN's Text is " +txtSSN.getAttribute("value") + " but should be " + ssn);
				extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
			}
			//steps to enter the homeaddress,city,state,zip code
			Thread.sleep(6000);
			EnterText(txtHomeAddress,homeaddr,"Home Address");
			
			EnterText(txtCity,city,"City");
			
			EnterText(txtState,state,"State");
		
			EnterText(txtZip,zip,"Zip Code");
			scrollIntoView(radiobtnMale, driver);
			Thread.sleep(1000);
			takeScreenshot("PersonalInformation_Down");
			takeScreenshot("HomePage_EOI_button");
			selectGender(gender,radiobtnMale,radiobtnFemale);
			EnterText(txtDOB,dob,"DOB");
			ComboSelectValue(txtMartialStatus,martialStatus,"Martial Status");
			EnterText(txtHeightFeet, heightFeet, "Height in feet");
			EnterText(txtHeightInches,heightinch,"Height in Inches");
			EnterText(txtWeight,weight,"Weight");
		
			EnterText(txtHireDate,doh,"Date Of Hire");
			EnterText(txtAnnualSalary,annualsalary,"Annual Salary");
			Thread.sleep(3000);
			//below code is for comparing the default value of phone in page with the value			
			try
			{	
		 		if(txtPhoneNumber.getAttribute("value").equalsIgnoreCase(phone))
		 		{
		 			extentTest.log(LogStatus.PASS,"Phone Number Text  is " +phone);
		 		}
		 		else
		 		{
		 			extentTest.log(LogStatus.FAIL, "Phone Number Text is " +txtPhoneNumber.getAttribute("value") + " but should be " +phone);
		 			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
		 		}
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL,"Phone Number Text is  " +txtPhoneNumber.getAttribute("value") + " but should be " +phone);
				extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
			}
			//Step to Select Phone Number Type
			ComboSelectValue(txtPhoneType,type,"Phone Number Type");
			
			//below code is for comparing the default value of Email in page with the value			
			try
			{	
		 		if(txtEmail.getAttribute("value").equalsIgnoreCase(email))
		 		{
		 			extentTest.log(LogStatus.PASS,"Email Address Text  is " +email);
		 		}
		 		else
		 		{
		 			extentTest.log(LogStatus.FAIL, "Email Address Text is " +txtEmail.getAttribute("value") + " but should be " +email);
		 			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
		 		}
			}catch(NoSuchElementException e)
			{
				e.printStackTrace();
				extentTest.log(LogStatus.FAIL,"Email Address Text is  " +txtEmail.getAttribute("value") + " but should be " +email);
				extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
			}
			assertElementDisplayed(btnBack,"Back button");
			assertElementDisplayed(btnSaveFinish,"Save & Finish button");
			assertElementDisplayed(btnSaveContinue,"Save & Continuse button");
			//assertTrue(btnBack.isDisplayed());
			//assertTrue(btnSaveFinish.isDisplayed());
			//assertTrue(btnSaveContinue.isDisplayed());
			ClickElement(radiobtnNo,"Radio Button No");
			ClickElement(btnSaveContinue,"Save and Continue Button");
			Thread.sleep(3000);
			return new CoverageChangeRequestPageAction();
		}
		
//		10/27 - Added by Pallavi for SSO user Employee Only Approved scenario
		
		public CoverageChangeRequestPageAction verifyPersonalInfoSSO (String ssnVal, String heightFt, String heightInch, String weightLbs) 
				throws InterruptedException, AWTException {
			
			assertText(headerPersonalInfo,"Personal Information","Step 1 Header");
			assertText(headerMyIdentification,"Employee Identification","Step 1 Subsection Header");
			Thread.sleep(2000);
			takeScreenshot("PersonalInformation_Up");
			Thread.sleep(3000);
			
//			Enter values for SSN, Height and Weight fields
			EnterText(txtSSN, ssnVal, "Employee SSN field");
//			tabOut(txtSSN, "Employee SSN field");
			Thread.sleep(3000);
			scrollIntoView(headerMyIdentification, driver);
			EnterText(txtHeightFeet, heightFt, "Height in feet");
			EnterText(txtHeightInches,heightInch,"Height in Inches");
			EnterText(txtWeight,weightLbs,"Weight in Lbs");
			Thread.sleep(3000);
			takeScreenshot("SSN_Height_Weight");
			scrollPageDown(driver);
			Thread.sleep(3000);
			ClickElement(radiobtnNo, "Nicotine - No Radio Button");
			Thread.sleep(3000);
			takeScreenshot("RadioBtnNO");
			assertElementDisplayed(btnBack,"Back button");
			assertElementDisplayed(btnSaveFinish,"Save & Finish button");
			assertElementDisplayed(btnSaveContinue,"Save & Continuse button");
			ClickElement(btnSaveContinue,"Save and Continue Button");
			Thread.sleep(3000);
			return new CoverageChangeRequestPageAction();		
			
		}
		
//		12/2 - Added by Pallavi for SSO user Spouse Only Referred scenario
		public CoverageChangeRequestPageAction verifyPersonalInfoSSOSpouseOnly (String ssnVal, String heightFt, String heightInch, 
				String weightLbs, String phNum, String phType) throws InterruptedException, AWTException {
			
			assertText(headerPersonalInfo,"Personal Information","Step 1 Header");
			assertText(headerSpouseIdentification,"Spouse/Domestic Partner Identification","Step 1 Subsection Header");
			Thread.sleep(2000);
			takeScreenshot("PersonalInformation_Up");
			Thread.sleep(3000);
			
//			Enter values for SSN, Height, Weight, Phone Number and Type fields
			EnterText(txtSSN, ssnVal, "Employee SSN field");
//			tabOut(txtSSN, "Employee SSN field");
			Thread.sleep(3000);
			scrollIntoView(headerSpouseIdentification, driver);
			EnterText(txtHeightFeet, heightFt, "Height in feet");
			EnterText(txtHeightInches,heightInch,"Height in Inches");
			EnterText(txtWeight,weightLbs,"Weight in Lbs");
			Thread.sleep(3000);
			takeScreenshot("SSN_Height_Weight");
			scrollPageDown(driver);
			
//			Enter Phone number
			String xpathPhoneNum = "//input[@id='phone-number']";
//			WebElement txtPhoneNum = driver.findElement(By.xpath("//input[@id='phone-number']"));
//			txtPhoneNum.sendKeys(phNum);   //This does not work because of Stale Element reference exception
			driver.findElement(By.xpath(xpathPhoneNum)).click();
			driver.findElement(By.xpath(xpathPhoneNum)).sendKeys(phNum);
			Thread.sleep(2000);
			WebElement txtPhoneNum = driver.findElement(By.xpath(xpathPhoneNum));
			displayValue(txtPhoneNum, "Entered Phone Number");
//			EnterText(txtPhoneNum, phNum, "Phone Number field");   //This does not work because of Stale Element reference exception
//			Select Phone Type
			ComboSelectValue(txtPhoneType, phType, "Phone Number Type");
			ClickElement(radiobtnNo, "Nicotine - No Radio Button");
			Thread.sleep(3000);
			takeScreenshot("RadioBtnNO");
			assertElementDisplayed(btnBack,"Back button");
			assertElementDisplayed(btnSaveFinish,"Save & Finish button");
			assertElementDisplayed(btnSaveContinue,"Save & Continuse button");
			ClickElement(btnSaveContinue,"Save and Continue Button");
			Thread.sleep(3000);
			return new CoverageChangeRequestPageAction();		
			
		}
		
		
}