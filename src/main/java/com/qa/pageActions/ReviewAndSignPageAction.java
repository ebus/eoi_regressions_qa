package com.qa.pageActions;


import static org.testng.Assert.assertTrue;

import com.qa.pages.ReviewAndSignPage;

public class ReviewAndSignPageAction extends ReviewAndSignPage{
	public ReviewAndSignPageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name :Review And Sign Page Action 
	  * Purpose : This class contains the action of  the Review And Sign Page
	  * History : Created by Anjali Johny on 07/20/2021 
  ***********************************************************************************/
	public DemoDocumentSignPageAction validateReviewPage() throws Exception {
		assertElementDisplayed(headerRevieSign,"Review and Sign Page Header");
		assertElementDisplayed(cbAgreement,"Agreement checkbox");
		//assertTrue(cbAgreement.isDisplayed());
		ClickElement(cbAgreement,"Agreement CheckBox");
		Thread.sleep(3000);
		takeScreenshot("ReviewPage_AgreementCheckBox");   //Added by Pallavi on 11/16
		assertElementDisplayed(btnBack,"Back button");
		assertElementDisplayed(btnSaveAndSign,"Save & Sign button");
		//assertTrue(btnBack.isDisplayed());
		//assertTrue(btnSaveAndSign.isDisplayed());
		ClickElement(btnSaveAndSign, "Save and Sign");
		Thread.sleep(4000);
		return new DemoDocumentSignPageAction();
	}
}
