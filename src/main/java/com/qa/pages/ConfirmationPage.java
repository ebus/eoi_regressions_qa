package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ConfirmationPage extends GenericFunction{
	/* ***********************************************************************************
	Class Name  : Confirmation Page
	Purpose     : This class contains all objects of Confirmation Page
	History     : 07/19/2021 by Anjali Johny
	**************************************************************************************/
	@FindBy(xpath="//h3[contains(text(),'CONFIRMATION')]")
	public WebElement headerConfirmation;
	@FindBy(xpath="//div[@id='body']//div[contains(text(),'Pre-Approved')]")
	public WebElement txtPreApproved;
	@FindBy(xpath="//div[@id='body']//div[contains(text(),'Referred')]")
	public WebElement txtReferred;
	@FindBy(xpath="//div[@id='body']//div[contains(text(),'Declined')]")
	public WebElement txtDeclined;
	@FindBy (xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'My Summary')]")
	public WebElement btnMySummary;
	
//	10/28 - Added by Pallavi for SSO user Approved scenario
	@FindBy(xpath="//div[@class='decision-complete']")
	public WebElement txtApprovedSSO;
	
	@FindBy(xpath="//div[@class='decision']")
	public WebElement txtReferredSSOSpouseOnly;
	
	
	
	public ConfirmationPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}