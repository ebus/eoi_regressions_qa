package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class UpdateSecurityInformationPage extends GenericFunction{
	/* ***********************************************************************************
	Class Name  : UpdateSecurityInformationPage
	Purpose     : This class contains all objects of UpdateSecurityInformation page
	History     : 07/02/2021 by Anjali Johny a
	**************************************************************************************/
	@FindBy(xpath="//h2[contains(text(),'Update Your Security Information')]")
	public WebElement headerUpdateSecInfo;
	
	@FindBy(xpath = "//div[contains(text(),'Invalid password. Please check formatting rules and try again.')]")
	public WebElement msgPassword_Validation;
	
	@FindBy(xpath = "//li[contains(@class,'password-length-min failed-requirement')]/span")
	public WebElement msgMin8Characters_Validation;
	
	@FindBy(xpath = "//li[contains(@class,'password-length-max failed-requirement')]/span")
	public WebElement msgMax20Characters_Validation;
	
	@FindBy(xpath = "//li[contains(@class,'password-characters failed-requirement')]")
	public WebElement msgPwdCaseAndAlphnumeric_Validation;
	
	@FindBy(xpath = "//li[contains(@class,'password-invalid-special failed-requirement')]/span")
	public WebElement msgPwdSpecialCharacters_Validation;
	
	@FindBy(xpath = "//li[contains(@class,'password-username failed-requirement')]/span")
	public WebElement msgPwdContainsUserID_Validation;
	
	@FindBy(xpath="//input[@class='form-control js-temp-password']")
	public WebElement txtTempPassword;
	
	@FindBy(xpath = "//li[contains(@class,'password-space failed-requirement')]/span")
	public WebElement msgPwdContainsSpace_Validation;
	
	@FindBy(xpath="//input[@id='passwd']")
	public WebElement txtNewPassword;
	
	@FindBy(xpath = "//div[@class='form-group row js-fp-confirm-passwd-group has-error']//input[@id='confirm-passwd']")
	public WebElement txtConfirmPassword;
	
	@FindBy(xpath = "//div[contains(text(),'Passwords do not match.')]")
	public WebElement msgPasswordNotMatch_Validation;
	
	@FindBy(xpath = "//input[contains(@class,'terms-conditions')]")
	public WebElement CheckBox;
	
	@FindBy(xpath = "//div[contains(@class,'terms-conditions-error')]")
	public WebElement msgFieldRequired_Validation;
	
	
	@FindBy(xpath = "//button[@type='submit']")
	public WebElement btnSubmit;
	
	@FindBy(xpath = "//div//h2")
	public WebElement headerCongratulations;
	

	
	public UpdateSecurityInformationPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
}