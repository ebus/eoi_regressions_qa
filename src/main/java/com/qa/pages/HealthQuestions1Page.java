package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HealthQuestions1Page extends GenericFunction{
	/* ***********************************************************************************
	Class Name  : HealthQuestions1Page
	Purpose     : This class contains all objects of Health Questions1(page
	History     : 07/19/2021 by Anjali Johny
	**************************************************************************************/
	@FindBy(xpath="//div[@class='main-content-inner']//div[contains(text(),'Health Questions')]")
	public WebElement headerHealthQuestionsStep;
	@FindBy(xpath="(//input[@value='No'])[1]")
	public WebElement radiobtnQuestion1a_NO;
	@FindBy(xpath="(//input[@value='Yes'])[1]")
	public WebElement radiobtnQuestion1a_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[2]")
	public WebElement radiobtnQuestion2a_Yes;
	@FindBy(xpath="(//input[@value='No'])[2]")
	public WebElement radiobtnQuestion2a_NO;
	
	@FindBy(xpath="(//input[@value='No'])[3]")
	public WebElement radiobtnQuestion2b_NO;
	@FindBy(xpath="(//input[@value='Yes'])[3]")
	public WebElement radiobtnQuestion2b_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[4]")
	public WebElement radiobtnQuestion2c_Yes;
	@FindBy(xpath="(//input[@value='No'])[4]")
	public WebElement radiobtnQuestion2c_NO;
	
	@FindBy(xpath="(//input[@value='Yes'])[5]")
	public WebElement radiobtnQuestion2d_Yes;
	@FindBy(xpath="(//input[@value='No'])[5]")
	public WebElement radiobtnQuestion2d_NO;
	
	@FindBy(xpath="(//input[@value='No'])[6]")
	public WebElement radiobtnQuestion2e_NO;
	@FindBy(xpath="(//input[@value='Yes'])[6]")
	public WebElement radiobtnQuestion2e_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[7]")
	public WebElement radiobtnQuestion2f_Yes;
	@FindBy(xpath="(//input[@value='No'])[7]")
	public WebElement radiobtnQuestion2f_NO;
	
	
	@FindBy(xpath="(//input[@value='No'])[8]")
	public WebElement radiobtnQuestion2g_NO;
	@FindBy(xpath="(//input[@value='Yes'])[8]")
	public WebElement radiobtnQuestion2g_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[9]")
	public WebElement radiobtnQuestion2h_Yes;
	@FindBy(xpath="(//input[@value='No'])[9]")
	public WebElement radiobtnQuestion2h_NO;
	
	@FindBy(xpath="(//input[@value='Yes'])[10]")
	public WebElement radiobtnQuestion2i_Yes;
	@FindBy(xpath="(//input[@value='No'])[10]")
	public WebElement radiobtnQuestion2i_NO;
	
	@FindBy(xpath="(//input[@value='Yes'])[11]")
	public WebElement radiobtnQuestion2j_Yes;
	@FindBy(xpath="(//input[@value='No'])[11]")
	public WebElement radiobtnQuestion2j_NO;
	
	
	@FindBy(xpath="(//input[@value='Yes'])[12]")
	public WebElement radiobtnQuestion2k_Yes;
	@FindBy(xpath="(//input[@value='No'])[12]")
	public WebElement radiobtnQuestion2k_NO;
	
	@FindBy(xpath="(//input[@value='No'])[13]")
	public WebElement radiobtnQuestion2l_NO;
	@FindBy(xpath="(//input[@value='Yes'])[13]")
	public WebElement radiobtnQuestion2l_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[14]")
	public WebElement radiobtnQuestion2m_Yes;
	@FindBy(xpath="(//input[@value='No'])[14]")
	public WebElement radiobtnQuestion2m_NO;
	
	@FindBy(xpath="(//input[@value='No'])[15]")
	public WebElement radiobtnQuestion2n_NO;
	@FindBy(xpath="(//input[@value='Yes'])[15]")
	public WebElement radiobtnQuestion2n_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[16]")
	public WebElement radiobtnQuestion2o_Yes;
	@FindBy(xpath="(//input[@value='No'])[16]")
	public WebElement radiobtnQuestion2o_NO;
	
	@FindBy(xpath="(//input[@value='No'])[17]")
	public WebElement radiobtnQuestion2p_NO;
	@FindBy(xpath="(//input[@value='Yes'])[17]")
	public WebElement radiobtnQuestion2p_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[18]")
	public WebElement radiobtnQuestion2q_Yes;
	@FindBy(xpath="(//input[@value='No'])[18]")
	public WebElement radiobtnQuestion2q_NO;
	
	@FindBy(xpath="(//input[@value='Yes'])[19]")
	public WebElement radiobtnQuestion2r_Yes;
	@FindBy(xpath="(//input[@value='No'])[19]")
	public WebElement radiobtnQuestion2r_NO;
	
	@FindBy(xpath="(//input[@value='Yes'])[20]")
	public WebElement radiobtnQuestion2s_Yes;
	@FindBy(xpath="(//input[@value='No'])[20]")
	public WebElement radiobtnQuestion2s_NO;
	
	@FindBy(xpath="(//input[@value='No'])[21]")
	public WebElement radiobtnQuestion2t_NO;
	@FindBy(xpath="(//input[@value='Yes'])[21]")
	public WebElement radiobtnQuestion2t_Yes;
	
	@FindBy(xpath="(//input[@value='Yes'])[22]")
	public WebElement radiobtnQuestion2u_Yes;
	@FindBy(xpath="(//input[@value='No'])[22]")
	public WebElement radiobtnQuestion2u_NO;
		
	//Updated on 10/06/2021
	//2a
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[1]")
	public WebElement txtTreatmentDetails2a;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[1]")
	public WebElement txtTreatmentDate2a;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[1]")
	public WebElement txtTreamentDoctore2a;
	
	//2b
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[2]")
	public WebElement txtTreatmentDetails2b;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[2]")
	public WebElement txtTreatmentDate2b;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[2]")
	public WebElement txtTreamentDoctore2b;
	
	//2c
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[3]")
	public WebElement txtTreatmentDetails2c;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[3]")
	public WebElement txtTreatmentDate2c;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[3]")
	public WebElement txtTreamentDoctore2c;
	
	//2d
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[4]")
	public WebElement txtTreatmentDetails2d;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[4]")
	public WebElement txtTreatmentDate2d;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[4]")
	public WebElement txtTreamentDoctore2d;
	
	//2e
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[5]")
	public WebElement txtTreatmentDetails2e;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[5]")
	public WebElement txtTreatmentDate2e;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[5]")
	public WebElement txtTreamentDoctore2e;
	
	//2f
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[6]")
	public WebElement txtTreatmentDetails2f;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[6]")
	public WebElement txtTreatmentDate2f;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[6]")
	public WebElement txtTreamentDoctore2f;
	
	//2g
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[7]")
	public WebElement txtTreatmentDetails2g;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[7]")
	public WebElement txtTreatmentDate2g;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[7]")
	public WebElement txtTreamentDoctore2g;
	
	
	//2h
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[8]")
	public WebElement txtTreatmentDetails2h;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[8]")
	public WebElement txtTreatmentDate2h;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[8]")
	public WebElement txtTreamentDoctore2h;
	
	//2i
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[9]")
	public WebElement txtTreatmentDetails2i;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[9]")
	public WebElement txtTreatmentDate2i;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[9]")
	public WebElement txtTreamentDoctore2i;
	
	//2j
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[10]")
	public WebElement txtTreatmentDetails2j;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[10]")
	public WebElement txtTreatmentDate2j;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[10]")
	public WebElement txtTreamentDoctore2j;
	
	//2k
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[11]")
	public WebElement txtTreatmentDetails2k;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[11]")
	public WebElement txtTreatmentDate2k;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[11]")
	public WebElement txtTreamentDoctore2k;
	
	//2l
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[12]")
	public WebElement txtTreatmentDetails2l;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[12]")
	public WebElement txtTreatmentDate2l;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[12]")
	public WebElement txtTreamentDoctore2l;
	
	//2m
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[13]")
	public WebElement txtTreatmentDetails2m;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[13]")
	public WebElement txtTreatmentDate2m;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[13]")
	public WebElement txtTreamentDoctore2m;
	
	//2n
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[14]")
	public WebElement txtTreatmentDetails2n;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[14]")
	public WebElement txtTreatmentDate2n;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[14]")
	public WebElement txtTreamentDoctore2n;
	
	//2o
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[15]")
	public WebElement txtTreatmentDetails2o;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[15]")
	public WebElement txtTreatmentDate2o;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[15]")
	public WebElement txtTreamentDoctore2o;

	//2p
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[16]")
	public WebElement txtTreatmentDetails2p;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[16]")
	public WebElement txtTreatmentDate2p;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[16]")
	public WebElement txtTreamentDoctore2p;
	
	//2q
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[17]")
	public WebElement txtTreatmentDetails2q;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[17]")
	public WebElement txtTreatmentDate2q;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[17]")
	public WebElement txtTreamentDoctore2q;
	
	//2r
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[18]")
	public WebElement txtTreatmentDetails2r;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[18]")
	public WebElement txtTreatmentDate2r;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[18]")
	public WebElement txtTreamentDoctore2r;
	
	//2s
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[19]")
	public WebElement txtTreatmentDetails2s;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[19]")
	public WebElement txtTreatmentDate2s;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[19]")
	public WebElement txtTreamentDoctore2s;
	
	//2t
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[20]")
	public WebElement txtTreatmentDetails2t;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[20]")
	public WebElement txtTreatmentDate2t;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[20]")
	public WebElement txtTreamentDoctore2t;
	
	//2u
	@FindBy(xpath="(//div[@class='treatment-details js-eoi-question-page-treatment-details-group']//textarea[@class='js-eoi-question-page-treatment-details'])[21]")
	public WebElement txtTreatmentDetails2u;
	@FindBy(xpath="(//div[@class='treatment-date js-eoi-question-page-treatment-date-group']//input[@class='js-eoi-question-page-treatment-date'])[21]")
	public WebElement txtTreatmentDate2u;
	@FindBy(xpath="(//div[@class='treatment-provider js-eoi-question-page-treatment-provider-group']//input[@class='js-eoi-question-page-treatment-provider'])[21]")
	public WebElement txtTreamentDoctore2u;
	
	
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Back')]")
	public WebElement btnBack;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Finish Later')]")
	public WebElement btnSaveFinish;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Continue')]")
	public WebElement btnSaveContinue;
	
	public HealthQuestions1Page() {
		super();
		PageFactory.initElements(driver, this);
	}
}