package com.qa.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HomePage extends GenericFunction{
	
	/* ***********************************************************************************
	Class Name  : HomePage
	Purpose     : This class contains all objects of Home page
	History     : 07/05/2021 by Anjali Johny
	**************************************************************************************/
	
	@FindBy(xpath="//div[@class='main-content-inner']//a[contains(text(),'Home')]")
	public WebElement linkHomepage;
	@FindBy(xpath="//div[@class='landing-page visible-lg']//h1[contains(text(),'Welcome to OneAmerica')]")
	public WebElement headerHomePage;
//	@FindBy(xpath="//div[@class='landing-page visible-lg']//button[contains(text(),'Begin EOI')]")
	@FindBy(xpath = "(//div[@class='pull-right']//button[contains(text(),'Begin')])[1]")
	public WebElement btnBeginEOI;
	@FindBy(xpath="//div[@class='landing-page visible-lg']//button[contains(text(),'Continue EOI')]")
	public WebElement btnContinueEOI;
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Pre-Approved')]")
	public WebElement lblPreApprovedStatus;
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Referred')]")
	public WebElement lblReferredStatus;
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Declined')]")
	public WebElement lblDeclinedStatus;
	@FindBy(xpath="//div[@id='sidebar']//li//a//i[@class='menu-icon fa fa-sign-out']")
	public WebElement linkLogOut;
	@FindBy(xpath="//div[@class='landing-page visible-lg']//button[contains(text(),'View Confirmation')]")
	public WebElement lblViewConfirmation;
	//Updated on 10/05/2021
	@FindBy(xpath="//span[@class='vertical-align-middle']")
	public WebElement lblEOIStatus;
	
//	10/28 - Added by Pallavi for SSO user Approved scenario
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Approved')]")
	public List<WebElement> listLblApprovedSSO; 
	
	@FindBy(xpath="//div/p/label[contains(text(), 'Evidence')]")
	public List<WebElement> listLblEOIProcessStatus;
	
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Referred')]")
	public List<WebElement> listLblReferredSSO;
	
	@FindBy(xpath="//div[@class='landing-page visible-md visible-sm']//button[contains(text(), 'View Confirmation')]")
	public WebElement lblViewConfirmationSpouseOnlySSO;
	
	@FindBy(xpath="//div/p/label[contains(text(), 'Evidence')]")
	public List<WebElement> listHeaderEOIProcess;
	
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Declined')]")
	public List<WebElement> listLblDeclinedSSO;
	
	
	
	
	
	public HomePage() {
		super();
		PageFactory.initElements(driver, this);
	}
}