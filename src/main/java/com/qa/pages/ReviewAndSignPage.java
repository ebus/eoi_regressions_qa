package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;
/* ***********************************************************************************
Class Name  : ReviewAndSignnPage
Purpose     : This class contains all objects of ReviewAndSignn page
History     : 07/20/2021 by Anjali Johny 
**************************************************************************************/
public class ReviewAndSignPage extends GenericFunction{
	@FindBy(xpath="//div[@class='main-content-inner']//div[contains(text(),'Review and Sign')]")
	public WebElement headerRevieSign;
	@FindBy(xpath="//div[@class='checkbox-container']//input[@class='js-eoi-signing-checkbox checkbox-input']")
	public WebElement cbAgreement;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Sign')]")
	public WebElement btnSaveAndSign;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Back')]")
	public WebElement btnBack;
	
	public ReviewAndSignPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}