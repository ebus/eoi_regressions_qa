package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class CoverageChangeRequestPage extends GenericFunction{
	/* ***********************************************************************************
	Class Name  : CoverageChangeRequest Page
	Purpose     : This class contains all objects of CoverageChangeReques page
	History     : 07/19/2021 by Anjali Johny
	**************************************************************************************/
	@FindBy(xpath="//div[@id='body']//div[contains(text(),'Coverage or Change Being Requested')]")
	public WebElement headerStep2;
	@FindBy(xpath="//div[@class='js-eoi-coverage-editor-employee-coverage']//table")
	public WebElement tblEmpcoverage;
	@FindBy(xpath="//div[@class='js-eoi-coverage-editor-employee-coverage']//table//th[1]")
	public WebElement txtRow1_Benefits;
	@FindBy(xpath="//div[@class='js-eoi-coverage-editor-employee-coverage']//table//th[2]")
	public WebElement txtRow2_Class;
	@FindBy(xpath="//div[@class='js-eoi-coverage-editor-employee-coverage']//table//th[3]")
	public WebElement txtRow3_CurrentCoverage;
	@FindBy(xpath="//div[@class='js-eoi-coverage-editor-employee-coverage']//table//th[4]")
	public WebElement txtRow4_ElectedCoverage;
	@FindBy(xpath="//div[@class='js-eoi-coverage-editor-employee-coverage']//td//input[@class='form-control js-eoi-coverage-editor-list-item--current-coverage-amount']")
	public WebElement txtCoverageAmount;
	@FindBy (xpath="//div[@class='js-eoi-coverage-editor-employee-coverage']//td//input[@class='form-control js-eoi-coverage-editor-list-item--elected-coverage-amount']")
	public WebElement txtElectedAmount;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Back')]")
	public WebElement btnBack;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Finish Later')]")
	public WebElement btnSaveFinish;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Continue')]")
	public WebElement btnSaveContinue;
	
//	11/04 - Added by Pallavi for SSO user Approved scenario
	@FindBy(xpath="//div[@class='js-eoi-coverage-page-coverage-panel']")
	public WebElement headerMyCoverageDetails;
	
	
	
	
	public CoverageChangeRequestPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}