package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class DemoDocumentSignPage extends GenericFunction {
	/* ***********************************************************************************
	Class Name  : DemoDocumentSignPage
	Purpose     : This class contains all objects of DemoDocumentSignPage
	History     : 07/19/2021 by Anjali Johny
	**************************************************************************************/
	@FindBy(id="disclosureAccepted")
	public WebElement cbDisclosureAccept;
	@FindBy(xpath="//label[@class='cb_label']")
	public WebElement cbDiclosureAccept;
	@FindBy(id="action-bar-btn-continue")
	public WebElement btnContinue;
	@FindBy(id="navigate-btn")
	public WebElement btnStart;
	@FindBy(xpath="//button[@id='tab-form-element-1fe90d77-ff1a-46dc-9be5-ee1b1c5aa146']//div[@class='signature-tab-content tab-button-yellow v2']")
	public WebElement iconSign;
	@FindBy(xpath="//div[@class='signature-tab-content tab-button-yellow v2']//div")
	public WebElement btnSignIcon;
	@FindBy(xpath="//h1[@id='adopt-dialog-title']//span[contains(text(),'Adopt Your Signature')]")
	public WebElement headerAdoptSign;
	@FindBy(xpath="//div[@id='adopt-dialog']//button[contains(text(),'Adopt and Sign')]")
	public WebElement btnAdoptSign;
	@FindBy(xpath="//div[@id='adopt-dialog']//button[contains(text(),'Cancel')]")
	public WebElement btnCancel;
	@FindBy(id="action-bar-btn-finish")
	public WebElement btnFinish;
	
	
	
	
	
	
	
	
	public DemoDocumentSignPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
}