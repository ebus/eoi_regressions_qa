package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HealthQuestions3Page extends GenericFunction{
	
	
	/* ***********************************************************************************
	Class Name  : HealthQuestions3Page
	Purpose     : This class contains all objects of Health Questions 3(Step 3 of 5) page
	History     : 07/19/2021 by Anjali Johny
	**************************************************************************************/

	@FindBy(xpath="//div[@class='main-content-inner']//div[contains(text(),'Health Questions (Continued)')]")
	public WebElement headerHealthQuestions3;
	
	@FindBy(xpath="(//input[@value='No'])[1]")
	public WebElement radiobtnQuestion4_NO;
	@FindBy(xpath="(//input[@value='No'])[2]")
	public WebElement radiobtnQuestion5_NO;
	@FindBy(xpath="(//input[@value='No'])[3]")
	public WebElement radiobtnQuestion6_NO;
	@FindBy(xpath="(//input[@value='No'])[4]")
	public WebElement radiobtnQuestion7_NO;
	@FindBy(xpath="(//input[@value='No'])[5]")
	public WebElement radiobtnQuestion8_NO;
	@FindBy(xpath="(//input[@value='No'])[6]")
	public WebElement radiobtnQuestion9_NO;
	@FindBy(xpath="(//input[@value='No'])[7]")
	public WebElement radiobtnQuestion10_NO;
	
	@FindBy(xpath="(//input[@value='Yes'])[1]")
	public WebElement radiobtnQuestion4_Yes;
	@FindBy(xpath="(//input[@value='Yes'])[2]")
	public WebElement radiobtnQuestion5_Yes;
	@FindBy(xpath="(//input[@value='Yes'])[3]")
	public WebElement radiobtnQuestion6_Yes;
	@FindBy(xpath="(//input[@value='Yes'])[4]")
	public WebElement radiobtnQuestion7_Yes;
	@FindBy(xpath="(//input[@value='Yes'])[5]")
	public WebElement radiobtnQuestion8_Yes;
	@FindBy(xpath="(//input[@value='Yes'])[6]")
	public WebElement radiobtnQuestion9_Yes;
	@FindBy(xpath="(//input[@value='Yes'])[7]")
	public WebElement radiobtnQuestion10_Yes;
	
	
	@FindBy(xpath="//div[@class='js-eoi-question-page-medication-list']//input[@class='name-value js-eoi-question-page-medication-name']")
	public WebElement txtMedication;
	@FindBy(xpath="//div[@class='js-eoi-question-page-medication-list']//input[@class='date-value js-eoi-question-page-medication-used']")
	public WebElement txtDate;
	@FindBy(xpath="//div[@class='js-eoi-question-page-medication-list']//input[@class='date-value js-eoi-question-page-medication-prescription-date']")
	public WebElement txtDatePrescribed;
	@FindBy(xpath="//div[@class='js-eoi-question-page-medication-list']//input[@class='prescriber-value js-eoi-question-page-medication-prescriber']")
	public WebElement txtNameAddr;
	@FindBy(xpath="//div[@class='additional-question js-eoi-question-page-additional-question']//label[contains(text(),'Additional Details')]")
	public WebElement lblAdditionalDetailQ;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//label[contains(text(),'Additional Details')])[1]")
	public WebElement lblAdditionalDetailQ5;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//label[contains(text(),'Additional Details')])[2]")
	public WebElement lblAdditionalDetailQ6;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//label[contains(text(),'Additional Details')])[3]")
	public WebElement lblAdditionalDetailQ7;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//label[contains(text(),'Additional Details')])[4]")
	public WebElement lblAdditionalDetailQ8;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//label[contains(text(),'Additional Details')])[5]")
	public WebElement lblAdditionalDetailQ9;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//label[contains(text(),'Additional Details')])[6]")
	public WebElement lblAdditionalDetailQ10;
	@FindBy(xpath="//div[@class='additional-question js-eoi-question-page-additional-question']//textarea[@class='js-eoi-question-page-additional-details']")
	public WebElement txtAdditionalDetailsQuestion;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//textarea[@class='js-eoi-question-page-additional-details'])[1]")
	public WebElement txtAdditionalDetailsQuestion5;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//textarea[@class='js-eoi-question-page-additional-details'])[2]")
	public WebElement txtAdditionalDetailsQuestion6;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//textarea[@class='js-eoi-question-page-additional-details'])[3]")
	public WebElement txtAdditionalDetailsQuestion7;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//textarea[@class='js-eoi-question-page-additional-details'])[4]")
	public WebElement txtAdditionalDetailsQuestion8;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//textarea[@class='js-eoi-question-page-additional-details'])[5]")
	public WebElement txtAdditionalDetailsQuestion9;
	@FindBy(xpath="(//div[@class='additional-question js-eoi-question-page-additional-question']//textarea[@class='js-eoi-question-page-additional-details'])[6]")
	public WebElement txtAdditionalDetailsQuestion10;
	
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Back')]")
	public WebElement btnBack;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Finish Later')]")
	public WebElement btnSaveFinish;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Continue')]")
	public WebElement btnSaveContinue;
	
	public HealthQuestions3Page() {
		super();
		PageFactory.initElements(driver, this);
	}
	
}