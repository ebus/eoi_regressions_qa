package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PersonalInformationPage extends GenericFunction{
	
	/* ***********************************************************************************
	Class Name  : Personal Information Page
	Purpose     : This class contains all objects of Personal Information page
	History     : 07/05/2021 by Anjali Johny
	**************************************************************************************/
	
	@FindBy(xpath="//div//h1[contains(text(),'Personal Information')]")
	public WebElement headerPersonalInfo;
	@FindBy(xpath="//div//h2[contains(text(),'Employee Identification')]")
	public WebElement headerMyIdentification;
	@FindBy(xpath="//div//h2[contains(text(),'Spouse/Domestic Partner Identification')]")
	public WebElement headerSpouseIdentification;
	@FindBy(xpath="//div[@class='personal-info-form']//input[@id='first-name']")
	public WebElement txtFname;
	@FindBy(xpath="//div[@class='personal-info-form']//input[@id='middle-name']")
	public WebElement txtMidname;
	@FindBy(xpath="//div[@class='personal-info-form']//input[@id='last-name']")
	public WebElement txtLastname;
	
//	@FindBy(id="ssn")
	@FindBy(xpath="//div//input[@id='ssn']")
	public WebElement txtSSN;
	@FindBy(id="address")
	public WebElement txtHomeAddress;
	@FindBy(id="city")
	public WebElement txtCity;
	@FindBy(id="state")
	public WebElement txtState;
	@FindBy(id="zip")
	public WebElement txtZip;
	@FindBy(id="radio-gender-male")
	public WebElement radiobtnMale;
	@FindBy(id="radio-gender-female")
	public WebElement radiobtnFemale;
	@FindBy(id="birth-date")
	public WebElement txtDOB;
	@FindBy(id="marital-status")
	public WebElement txtMartialStatus;
//	@FindBy(id="height-feet")
	@FindBy(xpath = "//div//input[@id='height-feet']") 
	public WebElement txtHeightFeet;
//	@FindBy(id="height-inches")
	@FindBy(xpath = "//div//input[@id='height-inches']")
	public WebElement txtHeightInches;
//	@FindBy(id="weight")
	@FindBy(xpath = "//div//input[@id='weight']")
	public WebElement txtWeight;
	@FindBy(id="occupation")
	public WebElement txtOccupation;
	
	
	
	
	//@FindBy(xpath="//div[@class='row-item personal-info-hire-date js-eoi-hire-date-group']//input[@id='hire-date']")
	@FindBy(id="hire-date")
	public WebElement txtHireDate;
	@FindBy(id="annual-salary")
	public WebElement txtAnnualSalary;
	@FindBy(id="phone-number")
	public WebElement txtPhoneNumber;
	@FindBy(id="phone-type")
	public WebElement txtPhoneType;
	@FindBy(xpath="//div[@class='personal-info-form']//input[@id='email']")
	public WebElement txtEmail;
	@FindBy(xpath="//div[@class='row-item js-eoi-nicotine-use-group']//input[@id='radio-nicotine-yes']")
	public WebElement radiobtnYes;
//	@FindBy(xpath="//div[@class='row-item js-eoi-nicotine-use-group']//input[@id='radio-nicotine-no']")
	@FindBy(xpath="//div//label[@for='radio-nicotine-no']")
	public WebElement radiobtnNo;
	@FindBy(xpath="//div[@class='personal-info-form']//input[@id='radio-spouse-requires-eoi-yes']")
	public WebElement radiobtnSpouseyes;
	@FindBy(xpath="//div[@class='personal-info-form']//input[@id='radio-spouse-requires-eoi-no']")
	public WebElement radiobtnSpouseno;
	@FindBy(xpath="//div//h2[contains(text(),'Spouse/Domestic Partner Email Address')]")
	public WebElement headerSpouse;
	@FindBy(xpath="//div[@class='js-eoi-dependent-email-wrapper']//input[@id='first-name']")
	public WebElement txtSpouseFname;
	@FindBy(xpath="//div[@class='js-eoi-dependent-email-wrapper']//input[@id='middle-name']")
	public WebElement txtSpouseMname;
	@FindBy(xpath="//div[@class='js-eoi-dependent-email-wrapper']//input[@id='last-name']")
	public WebElement txtSpouselname;
	@FindBy(xpath="//div[@class='js-eoi-dependent-email-wrapper']//input[@id='email']")
	public WebElement txtSpouseEmail;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Back')]")
	public WebElement btnBack;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Finish Later')]")
	public WebElement btnSaveFinish;
//	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Continue')]")
	@FindBy(xpath = "//div[contains(@class,'hid')]//button[contains(text(),'Conti')]")
	public WebElement btnSaveContinue;
	
//	@FindBy(xpath="//input[@id='phone-number']")
//	public WebElement txtPhNum;
	
	public PersonalInformationPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
