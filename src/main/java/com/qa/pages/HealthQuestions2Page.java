package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HealthQuestions2Page extends GenericFunction{
	/* ***********************************************************************************
	Class Name  : HealthQuestions2Page
	Purpose     : This class contains all objects of Health Questions 2(Step 3 of 5) page
	History     : 07/19/2021 by Anjali Johny
	**************************************************************************************/
	public HealthQuestions2Page(){
		super();
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//div[@class='main-content-inner']//div[contains(text(),'Health Questions (Continued)')]")
	public WebElement headerHealthQuestions2;
	@FindBy(xpath="//div[@id='body']//div[contains(text(),'Health Questions (Continued)')]")
	public WebElement headerHealthQuestion2a;
	@FindBy(xpath="(//input[@value='No'])[1]")
	public WebElement radiobtnQuestion3a_NO;
	@FindBy(xpath="(//input[@value='No'])[2]")
	public WebElement radiobtnQuestion3b_NO;
	@FindBy(xpath="(//input[@value='No'])[3]")
	public WebElement radiobtnQuestion3c_NO;
	@FindBy(xpath="(//input[@value='No'])[4]")
	public WebElement radiobtnQuestion3d_NO;
	@FindBy(xpath="(//input[@value='No'])[5]")
	public WebElement radiobtnQuestion3e_NO;
	@FindBy(xpath="(//input[@value='No'])[6]")
	public WebElement radiobtnQuestion3f_NO;
	@FindBy(xpath="(//input[@value='No'])[7]")
	public WebElement radiobtnQuestion3g_NO;
	@FindBy(xpath="(//input[@value='No'])[8]")
	public WebElement radiobtnQuestion3h_NO;
	@FindBy(xpath="(//input[@value='No'])[9]")
	public WebElement radiobtnQuestion3i_NO;
	@FindBy(xpath="(//input[@value='No'])[10]")
	public WebElement radiobtnQuestion3j_NO;
	@FindBy(xpath="(//input[@value='No'])[11]")
	public WebElement radiobtnQuestion3k_NO;
	@FindBy(xpath="(//input[@value='No'])[12]")
	public WebElement radiobtnQuestion3l_NO;
	@FindBy(xpath="(//input[@value='No'])[13]")
	public WebElement radiobtnQuestion3m_NO;
	@FindBy(xpath="(//input[@value='No'])[14]")
	public WebElement radiobtnQuestion3n_NO;
	
	
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Back')]")
	public WebElement btnBack;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Finish Later')]")
	public WebElement btnSaveFinish;
	@FindBy(xpath="//div[@class='navigation-button-container hidden-sm hidden-xs']//button[contains(text(),'Save & Continue')]")
	public WebElement btnSaveContinue;
	
	
	
	
	
	
}